% Marcel Klett
\subsection{Verschwinden der Matrixelemente}

\paragraph{a) Anfangs- und Endzustände}

Ein konkretes Matrixelement \[ \braket{f|S^{(1)}|i} \] verschwindet
nur dann nicht, wenn $\ket{i}$ und $\ket{f}$ die Teilchen enthalten,
die in~\eqref{eq:57} erzeugt oder vernichtet werden. Als Beispiel
betrachten wir die Zustände
\begin{subequations}
  \begin{align}
    \label{eq:58a} 
    \ket{i} &= a^\dagger(\bm{k}_1,\lambda_1) \ket{0}\\
    \ket{f} &= b^\dagger(\bm{k}'_2,s'_2) d^\dagger(\bm{k}'_3,s'_3) \ket{0}  . 
    \label{eq:58b}
  \end{align} 
\end{subequations}
Dabei verbleibt nur der zweite Term aus~\eqref{eq:57}. Er enthält die
Operatoren $b^\dagger d^\dagger a$ und damit
\begin{equation}
  \label{eq:59}
  \braket{\underbrace{0|db}_{\bra{f}} \left( d^\dagger b^\dagger a \right) \underbrace{a^\dagger |0}_{\ket{i}}} \neq 0
\end{equation}
für geeignete $k,\lambda$ und $s$. Betrachten wir hingegen den ersten
Term aus~\eqref{eq:57}, so enthält dieser das Matrixelement
\begin{equation}
  \label{eq:510}
  \braket{0|db (b^\dagger a b)  a^\dagger|0} = 0 \mcomma
\end{equation}
wobei das Ergebnis trivial zu sehen ist, denn die Operatoren
$a^\dagger$ und $b$ vertauschen und somit wenden wir einen
Vernichtungsoperator auf den Vakuumzustand an.

Der zweite Prozess aus Gleichung~\eqref{eq:59} heißt Paarerzeugung,
dabei verwandelt sich ein Photon in ein Elektron-Positron-Paar.
\begin{center}
  \begin{tikzpicture}
    \matrix[feyn={.7,.5}] (m) {
      |(a)| &               & |(b)| \\
            & |[vertex](c)| &       \\
      |(d)| &               &       \\
    };
    \draw[photon]  (d) to (c);
    \draw[fermion] (a) to (c);
    \draw[fermion] (c) to (b);
  \end{tikzpicture}
\end{center}

\paragraph{b) Energie- und Impulserhaltung}

Dass es geeignete Anfangs- und Endzustände gibt, heißt noch nicht,
dass der Beispielprozess auch existiert. Das Matrixelement muss
ungleich Null sein. Eine explizite Rechnung (vgl. Übungen) für einen
ähnlichen Prozess liefert
\begin{align}
  \begin{aligned}
    &\braket{0|d(\bm{k}'_3,s_3') b(\bm{k}'_2,s_2') (-\ii e/\hbar) \int \overline{\psi}_{b^\dagger} \gamma^\alpha  \underline{\psi}_{d^\dagger} A_{a \alpha} \diff^4 x \; | a^\dagger(\bm{k}_1,\lambda_1) |0} \\
    &= \frac{- \ii e m c^3 \sqrt{\mu_0} \, \overline{u}(\bm{k}'_2,s'_2) \gamma^\alpha \underline{v}(\bm{k}'_3,s'_3) \varepsilon_\alpha(\bm{k}_1,\lambda_1)}{\sqrt{4 \pi \hbar E_{\bm{k}'_3} E_{\bm{k}'_2} \omega_{\bm{k}_1}}} \delta^{(4)}(\bm{k}'_3 + \bm{k}'_2 - \bm{k}_1) \mpunkt
  \end{aligned}
  \label{eq:511}
\end{align}
Die $\delta$-Funktion fordert die Erhaltung des Viererimpulses am
Vertex (also die Energie- und Impulserhaltung). Im vorliegenden Fall
lässt sich beides nicht gleichzeitig erfüllen. Gleiches gilt für alle
acht Prozesse aus~\eqref{eq:57}, das heißt alle Prozesse erster
Ordnung existieren in der Natur nicht.

\subsection{Externe elektromagnetische Felder}

In Fällen, in denen das elektromagnetische Feld in guter Näherung als
äußeres klassisches Feld behandelt werden kann, sind Prozesse erster
Ordnung möglich, zum Beispiel die Mott-Streuung
\begin{equation}
  \label{eq:512}
  S^{(1)} = (-\ii e/\hbar) \int \overline{\psi}_{b^\dagger} \gamma^\alpha A_\alpha(\bm{r}) \underline{\psi}_b \diff^4 x \mcomma
\end{equation}
wobei hier ein klassisches Viererpotential der Form $ A = (\phi/c, 0
,0,0)$ angenommen wird.
\begin{center}
  \begin{tikzpicture}
    \matrix[feyn] (m) {
      |(a)| &               & &              \\
            & |[vertex](b)| & & |[cross](c)| \\
      |(d)| &               & &              \\
    };
    \draw[photon] (b) to (c);
    \draw[fermion] (d) to (b);
    \draw[fermion] (b) to (a);
  \end{tikzpicture}  
\end{center}
Hierbei handelt es sich im eigentlichen Sinne um keinen Prozess erster
Ordnung, da die Annahme eines klassischen Feldes nur dann
gerechtfertigt ist, sofern eine hohe Anzahl von Photonen vorliegt. In
der ersten Ordnung ist jedoch nur ein Photon involviert.

\section{Feynman-Regeln}

Gleichung~\eqref{eq:511} lässt erkennen, welche Ausdrücke aus den
Feldoperatoren nach Auswertung aller Integrale im Ergebnis
auftreten. Die Feldoperatoren können nach~\eqref{eq:52} jedoch
einzelnen Ausdrücken aus den Feynman-Diagrammen zugeordnet werden. Das
funktioniert auch für höhere Ordnungen systematisch und
eindeutig. Daher gibt es feste Regeln, nach denen man ein
Feynman-Diagramm in ein Element der Streumatrix in der
Form~\eqref{eq:511} umschreiben kann und umgekehrt. Dies erspart eine
explizite Rechnung. Dabei lauten die Feynman-Regeln wie folgt:
\begin{itemize}
\item Die Übergangsamplitude $\ket{i} \to \ket{f}$ ist durch eine Störungsreihe
  \begin{equation}
    \label{eq:513}
    S_{fi} = \braket{f|S|i} = \delta_{fi} + \sum_{n=1}^{\infty} S^{(n)}_{fi}
  \end{equation}
gegeben.
\item In der Ordnung $S^{(n)}_{fi}$ treten alle topologisch
  verschiedenen Feynman-Diagramme $n$-ter Ordnung ($n$ Vertizes)
  auf. Jedes dieser Feynman-Diagramme ergibt eine Teilamplitude
  $S_{fi,m}$ mit
  \begin{equation}
    \label{eq:514}
    S^{(n)}_{fi} = \sum_m S^{(n)}_{fi,m} \mpunkt
  \end{equation}
\end{itemize}
Die Teilamplitude lässt sich nach folgenden Regeln aus dem Diagramm
auslesen, wobei alle Terme multiplikativ auftreten. Die Terme innerer
Fermionenlinien werden entgegen der Pfeilrichtung aus dem Diagramm
abgelesen und dann von links nach rechts geschrieben.
\begin{itemize}
\item Elektron
  \begin{align*}
    \begin{tikzpicture}[baseline={([yshift=\dimexpr-\fontdimen22\textfont2\relax]m.center)}]
      \matrix[feyn,ampersand replacement=\&] (m) {
              \& |[vertex](c)| \\
        |(d)| \&               \\
      };
      \draw[fermion] (d) to (c);
    \end{tikzpicture}
    &=\frac{1}{\sqrt{2\pi}^3} \sqrt{\frac{mc^2}{E_{\bm{k}}}} \underline{u}(\bm{k},s)
    && \text{einlaufend} \\
    \begin{tikzpicture}[baseline={([yshift=\dimexpr-\fontdimen22\textfont2\relax]m.center)}]
      \matrix[feyn,ampersand replacement=\&] (m) {
                      \& |(d)| \\
        |[vertex](c)| \&       \\
      };
      \draw[fermion] (c) to (d);
    \end{tikzpicture} 
    &=\frac{1}{\sqrt{2\pi}^3} \sqrt{\frac{mc^2}{E_{\bm{k}}}} \overline{u}(\bm{k},s)
    && \text{auslaufend}
  \end{align*}

\item Positron
  \begin{align*}
    \begin{tikzpicture}[baseline={([yshift=\dimexpr-\fontdimen22\textfont2\relax]m.center)}]
      \matrix[feyn,ampersand replacement=\&] (m) {
              \& |[vertex](c)| \\
        |(d)| \&               \\
      };
      \draw[fermion] (c) to (d);
    \end{tikzpicture}
    &=\frac{1}{\sqrt{2\pi}^3} \sqrt{\frac{mc^2}{E_{\bm{k}}}} \overline{v}(\bm{k},s)
    && \text{einlaufend} \\
    \begin{tikzpicture}[baseline={([yshift=\dimexpr-\fontdimen22\textfont2\relax]m.center)}]
      \matrix[feyn,ampersand replacement=\&] (m) {
                      \& |(d)| \\
        |[vertex](c)| \&       \\
      };
      \draw[fermion] (d) to (c);
    \end{tikzpicture} 
    &=\frac{1}{\sqrt{2\pi}^3} \sqrt{\frac{mc^2}{E_{\bm{k}}}} \underline{v}(\bm{k},s)
    && \text{auslaufend}
  \end{align*}

\item Photon
  \begin{equation*}
    \begin{tikzpicture}[baseline={([yshift=\dimexpr-\fontdimen22\textfont2\relax]m.center)}]
      \matrix[feyn,ampersand replacement=\&] (m) {
              \& |[vertex](c)| \\
        |(d)| \&               \\
      };
      \draw[photon] (c) to (d);
    \end{tikzpicture}
    = \frac{1}{\sqrt{2\pi}^3} \sqrt{\frac{\hbar \mu_0 c^2}{2 \omega_{\bm{k}}}} \varepsilon_{\alpha}(\bm{k},\lambda) =
    \begin{tikzpicture}[baseline={([yshift=\dimexpr-\fontdimen22\textfont2\relax]m.center)}]
      \matrix[feyn,ampersand replacement=\&] (m) {
                      \& |(d)| \\
        |[vertex](c)| \&       \\
      };
      \draw[photon] (d) to (c);
    \end{tikzpicture} 
  \end{equation*}

\item $\text{Vertex} = -\frac{\ii e}{\hbar} (2\pi)^4 \gamma^\alpha \delta^{(4)}(\sum_{i,\textrm{ausl.}} k_i - \sum_{i,\textrm{einl.}} k_i)$

\item Innere Fermionenlinie
  \begin{equation*}
    \begin{tikzpicture}[baseline={([yshift=\dimexpr-\fontdimen22\textfont2\relax]m.center)}]
      \matrix[feyn] (m) {
        |[vertex](a)|  & &  |[vertex](d)| \\  
      };
      \draw[fermion] (a) to (d); 
    \end{tikzpicture}
    = \ii \int \frac{\diff^4 k}{(2\pi)^4} \tilde{S}_F(k)
    = \int \frac{\diff^4 k}{(2\pi)^4} \frac{\slashed{k}
      + \frac{mc}{\hbar}}{k_\mu k^\mu - (mc/\hbar)^2 + \ii \varepsilon} \mcomma
  \end{equation*}
  wobei die Impulse gegebenenfalls nicht durch ein- und auslaufende
  Zustände festgelegt sind.

\item Innere Photonenlinie
  \begin{equation*}
    \begin{tikzpicture}[baseline={([yshift=\dimexpr-\fontdimen22\textfont2\relax]m.center)}]
      \matrix[feyn] (m) {
        |[vertex](a)|  & &  |[vertex](d)|  \\  
      };
      \draw[photon] (a) to (d); 
    \end{tikzpicture}
    = \ii \int \frac{\diff^4 k}{(2\pi)^4} \tilde{D}_{F_{\alpha\beta}}(k)
    = - \ii \int \frac{\diff^4 k}{(2\pi)^4} \frac{\hbar \mu_0 c \eta_{\alpha \beta}}{k_\mu k^\mu + \ii \varepsilon}
  \end{equation*}

\item Ist eine ungerade Anzahl an Transpositionen nötig, um die
  Fermionenoperatoren in Normalordnung zu bringen, so wird das
  Ergebnis mit $-1$ durchmultipliziert.
\item Für jede in sich geschlossene Fermionenschleife
  \begin{tikzpicture}[baseline=\dimexpr-\fontdimen22\textfont2\relax]
    \matrix[feyn] (m) {
      |[vertex](a)| & &  |[vertex](b)| \\
    };
    \draw[fermion,floop] (a) to (b);
    \draw[fermion,floop] (b) to (a);
  \end{tikzpicture}
  wird das Ergebnis ebenfalls mit $-1$ durchmultipliziert.
\end{itemize}

\section{Ausgewählte Prozesse 2. Ordnung}

\subsection{Streumatrix 2. Ordnung}

Die Streumatrix zweiter Ordnung lautet umgeschrieben in ein normalgeordnetes Produkt
\begin{align}
  \begin{aligned}
    S^{(2)} = \frac{1}{2} \left( - \frac{\ii e}{\hbar} \right)^2 \int \diff x_1 \diff x_2 &\Bigl\{ \nOrd{\overline{\psi}(x_1) \gamma^\alpha A_\alpha(x_1) \underline{\psi}(x_1) \overline{\psi}(x_2) \gamma^\beta A_\beta(x_2) \underline{\psi}(x_2) }   \\
    &+ \, \nOrd{\underbracket{\overline{\psi}(x_1) \gamma^\alpha A_\alpha(x_1) \underline{\psi}(x_1) \overline{\psi}(x_2) \gamma^\beta A_\beta(x_2) \underline{\psi}(x_2)}} \\
    &+ \, \nOrd{\overline{\psi}(x_1) \gamma^\alpha \underbracket{A_\alpha(x_1) \underline{\psi}(x_1) \overline{\psi}(x_2) \gamma^\beta A_\beta(x_2)} \underline{\psi}(x_2)} \\
    &+ \, \nOrd{\overline{\psi}(x_1) \gamma^\alpha A_\alpha(x_1) \underbracket{\underline{\psi}(x_1) \overline{\psi}(x_2)} \gamma^\beta A_\beta(x_2) \underline{\psi}(x_2)} \\
    &+ \, \nOrd{\underbracket{\overline{\psi}(x_1) \gamma^\alpha \underbracket{A_\alpha(x_1) \underline{\psi}(x_1) \overline{\psi}(x_2) \gamma^\beta A_\beta(x_2)} \underline{\psi}(x_2)}} \\
    &+ \, \nOrd{\underbracket{\overline{\psi}(x_1) \gamma^\alpha A_\alpha(x_1) \underbracket{\underline{\psi}(x_1) \overline{\psi}(x_2)} \gamma^\beta A_\beta(x_2) \underline{\psi}(x_2)}} \\
    &+ \, \nOrd{\overline{\psi}(x_1) \gamma^\alpha \underbracket{A_\alpha(x_1) \underbracket{\underline{\psi}(x_1) \overline{\psi}(x_2)} \gamma^\beta A_\beta(x_2)} \underline{\psi}(x_2)} \\
    &  + \underbracket{\overline{\psi}(x_1) \gamma^\alpha \underbracket{A_\alpha(x_1) \underbracket{\underline{\psi}(x_1) \overline{\psi}(x_2)} \gamma^\beta A_\beta(x_2)} \underline{\psi}(x_2)} \Bigr\} \mcomma \\
  \end{aligned}
  \label{eq:515}
\end{align}
wobei berücksichtigt wurde, dass kommutierende/antikommutierende Operatoren eine verschwindende Kontraktion haben.

\subsection{Vorkommende Prozesse}
Sortiert nach den Zeilen von~\eqref{eq:515}:
\begin{itemize}
\item 1. Zeile: Verdopplung der Prozesse erster Ordnung und damit kein
  Beitrag zur Übergangsamplitude
\item 2. und 4. Zeile: Prozesse mit einer inneren Fermionenlinie, zum
  Beispiel die Elektron-Photon-Streuung
  \begin{center}
    \begin{tikzpicture}
      \matrix[feyn] (m) {
        |(a)| &               &                & |(b)| \\
              & |[vertex](c)| &  |[vertex](d)| &       \\
        |(e)| &               &                & |(f)| \\
      };
      \draw[fermion] (e) to (c);
      \draw[photon]  (a) to (c);
      \draw[fermion] (c) to (d);
      \draw[fermion] (d) to (b);
      \draw[photon]  (d) to (f);
    \end{tikzpicture}    
  \end{center}

\item 3. Zeile: Prozesse mit einer inneren Photonenlinie, zum Beispiel
  die Elektron-Positron-Streuung (Bhaba-Streuung)
  \begin{center}
    \begin{tikzpicture}
      \matrix[feyn] (m) {
        |(a)| &               &                & |(b)| \\
              & |[vertex](c)| &  |[vertex](d)| &       \\
        |(e)| &               &                & |(f)| \\
      };
      \draw[fermion] (e) to (c);
      \draw[fermion] (c) to (a);
      \draw[photon]  (c) to (d);
      \draw[fermion] (b) to (d);
      \draw[fermion] (d) to (f);
    \end{tikzpicture}
  \end{center}
\item 5.-7.\ Zeile: Prozesse mit zwei inneren Linien, zum Beispiel die
  Vakuumpolarisation (virtuelles Elektron-Positron-Paar)
  \begin{center}
    \begin{tikzpicture}
      \matrix[feyn] (m) {
              &               &                & |(a)| \\
              & |[vertex](b)| &  |[vertex](c)| &       \\
        |(d)| &               &                &       \\
      };
      \draw[photon]  (d) to (b);
      \draw[fermion,floop] (b) to (c);
      \draw[fermion,floop] (c) to (b);
      \draw[photon]  (c) to (a);
    \end{tikzpicture}
  \end{center}
\item 8.Zeile: Sog.\ Vakuumblase, welche einen divergierenden Anteil
  liefert und daher ignoriert werden muss.
  \begin{center}
    \begin{tikzpicture}
      \matrix[feyn] (m) {
        |[vertex](a)| &  & |[vertex](b)| \\
      };
      \draw[fermion,floop] (a) to (b);
      \draw[fermion,floop] (b) to (a);
      \draw[photon]  (a) to (b);
    \end{tikzpicture}
  \end{center}
\end{itemize}

