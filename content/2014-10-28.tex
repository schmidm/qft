% Marcel Klett

\subsection{Bedeutung der Zustände negativer Energie}

Die Zustände positiver Energie beschreiben die gewünschten
Spin-$1/2$-Teilchen. Doch die Zustände negativer Energien lassen sich
nicht ignorieren, denn sobald die Teilchen nicht mehr ruhen, gibt es
Kopplungen zwischen den Lösungen positiver und negativer
Energie. Zudem besteht -- wie in der Klein-Gordon-Gleichung -- das
Problem des unbeschränkten negativen Energiespektrums, was keinen
stabilen Grundzustand zur Folge hätte.

\paragraph{a) Vorschlag von Dirac:}

Alle Zustände negativer Energien sind besetzt. Da es sich
um Spin-$1/2$-Teilchen, also um Fermionen handelt, können keine
Zerfälle in diese Zustände stattfinden. Der Vakuumszustand ist dann
ein Dirac-See bestehend aus Teilchen mit negativer Energie. Um einen
angeregten Zustand zu erhalten, wird ein Teilchen aus dem Dirac-See
(negative Energie) in den Bereich positiver Energie angehoben
(vgl. Abbildung~\ref{fig:2014-10-28-1}). Der besetzte Zustand positiver Energie entspricht dann einem Elektron, die fehlende Besetzung eines Zustandes negativer Energie (Loch) ist ein Positron (Antiteilchen). Dieser Übergang in einen angeregten
Zustand wird auch \acct{Paarbildung} genannt.
\begin{figure}[tb]
  \begin{tikzpicture}[gfx]
    \draw[->] (-2.5,0) -- (-2.5,3) node[left] {$E$};
    \begin{scope}[shift={(-2.5,1.5)}]
      \draw (2pt,0) -- (-2pt,0) node[left] {$0$};
    \end{scope}
    \draw[pattern=north east lines] (-2,0) to[bend left] node[dot,pos=.8] (A) {} (2,0);
    \draw (-2,3) to[bend right] node[pos=.8] (B) {} (2,3);
    \draw[->] (A) -- (B);
    \draw[DarkOrange3,<-,decorate,decoration={snake,pre length=2mm}] (A) -- node[pos=.3,below right] {$\gamma$} +(45:1.5);
  \end{tikzpicture}
  \caption{Ein angeregter Zustand entsteht durch anheben eines Teilchens
    aus dem Dirac-See in den positiven Energiebereich.}
  \label{fig:2014-10-28-1}
\end{figure}

Es zeigen sich jedoch Probleme dieser Interpretation. Der
Grundzustand besitzt weiterhin die Energie $E = -\infty$. Zudem gibt
es Probleme mit der Unschärferelation. Gehen wir davon aus, dass ein
gut lokalisiertes Teilchen durch eine Ortsmessung gegeben ist,
\begin{equation}
  \label{eq:2014-10-28-1}
  \Delta x < \frac{\hbar}{4 m c} \mcomma
\end{equation}
dann erhält man für die Impulsunschärfe
\begin{equation}
  \label{eq:2014-10-28-2}
  \Delta p \geq 2 m c \mcomma
\end{equation}
was auf eine Energieunschärfe von $\Delta E \approx c \Delta p >
2mc^2$ führt und somit genügend Energie aufbringt, eine Paarbildung
stattfinden zulassen. Das Bild des Dirac-Sees ist zudem nur für
Fermionen und nicht für Bosonen geeignet. Außerdem zeigen sich
Asymmetrien zwischen Positronen und Elektronen.

\paragraph{b) Feynman-Stückelberg-Interpretation:}

Die Lösungen der Dirac-Gleichung für ein Teilchen mit Impuls sind von der Form
\begin{equation}
  \label{eq:2014-10-28-3}
  \bm{\psi}_+ = \bm{\psi}_0 \ee^{-\ii k_\mu x^\mu} \mcomma \quad  \bm{\psi}_- = \bm{\psi}_0 \ee^{\ii k_\mu x^\mu} \mpunkt
\end{equation}
Umschreiben der Lösung negativer Energie führt auf eine
Exponentialfunktion der Form
\begin{equation}
  \label{eq:2014-10-28-4}
  \bm{\psi}_- = \bm{\psi}_0 \ee^{\ii k_\mu x^\mu} = \bm{\psi}_0 \ee^{-\ii k_\mu (-x^\mu)} 
\end{equation}
Damit besitzen die Antiteilchen auch positive Energien, bewegen sich
aber im gespiegelten Raum ($\bm{r} \to -\bm{r}$) rückwärts in der Zeit
($t \to -t$). Diese Interpretation liefert einige Vorteile:
\begin{itemize}
\item keine negativen Energien,
\item kein Problem mit der Unschärferelation,
\item funktioniert für Bosonen und Fermionen.
\end{itemize}
Dennoch ist sie nicht intuitiv und nur schwer gedanklich
nachzuverfolgen.

\subsection{Im elektromagnetischen Feld}

Mit dem Viererpotential
\begin{equation}
  \label{eq:144}
  A_\mu =
  \begin{pmatrix}
    \phi/c \\
    -A_x \\
    -A_y \\
    -A_z \\
  \end{pmatrix}
\end{equation}
lautet die Dirac-Gleichung
\begin{align}
  \begin{aligned}
    \left[ \gamma^\mu ( \ii \partial_\mu - e  A_\mu) - \frac{m c}{\hbar} \right] \underline\psi &= 0 \\
    \text{oder} \quad \left[ \ii  \slashed{\partial} - e \slashed{A} - \frac{m c }{\hbar} \right] \underline\psi &= 0 \mpunkt
  \end{aligned}
  \label{eq:145}
\end{align}
Mit dieser Gleichung ist die relativistische Beschreibung des
Wasserstoffatoms möglich. Im Moment treten die Felder als klassische
Größen auf, es ist jedoch möglich und erforderlich (Photoeffekt), die
Felder zu quantisieren. Quantisieren wir jedoch die elektrischen und
magnetischen Felder in \eqref{eq:145}, so werden zwei Sichtweisen der
Quantenmechanik vermischt.
\begin{itemize}
\item Elektrodynamik: Quantisierung eines Feldes, höhere Anregung
  bedeutet höhere Teilchenzahl (Photonen).
\item Materie: Quantisierung von Punktteilchen, jedes Teilchen
  benötigt eine Wellenfunktion und eine eigene oder gekoppelte
  Dirac-Gleichung.
\end{itemize}
Die Dirac-Gleichung lässt im Allgemeinen einige Wünsche offen. Zum
einen müssen negative Energien interpretiert werden, zum anderen wäre
eine einheitliche Theorie für die Materie und ihre Wechselwirkungen
hilfreich. Diese einheitliche Theorie sollte, wenn möglich, auch die
Postulate der Quantenmechanik, nämlich
\begin{itemize}
\item Symmetrisierung/Antisymmetrisierung der Wellenfunktion für
  Bosonen/Fermionen
\item Forderung nach einer Normierung der Wellenfunktion für eine
  Teilchenzahl
\end{itemize}
auf einen festen Grund stellen. Es wird daher notwendig
Quantenfeldtheorien aufzustellen.

\chapter{Felder und deren Quantisierung}

\section{Ein Beispiel aus der Mechanik}

Wir betrachten eine schwingende Saite als verbundene Massepunkte der Masse $m$.
\begin{figure}[tb]
  \centering
  \begin{tikzpicture}[gfx]
    \draw[<->] (0,2) node[left] {$y$} |- (4,0) node[right] {$x$};
    \foreach \i/\j in {1/0, 2/1.5, 3/.6} {
      \node[MidnightBlue,dot] (n\i) at (\i,\j) {};
      \draw[dashed] (\i,0) -- (n\i);
    }
    \begin{scope}[MidnightBlue]
      \draw[dotted] (.5,.3) -- (n1) (n3) -- ++(.5,-.5);
      \draw (n1) -- (n2) -- (n3);
      \node[below] at (n1) {$y_{i-1}$};
      \node[above] at (n2) {$y_i$};
      \node[above right] at (n3) {$y_{i+1}$};
    \end{scope}
  \end{tikzpicture}
  \caption{Schwingende Saite.}
  \label{fig:2014-10-28-2}
\end{figure}
Lassen wir den Abstand $d$ zwischen den einzelnen Massepunkten gegen
$0$ laufen, so wird das System kontinuierlich (vgl.\ Abbildung~\ref{fig:2014-10-28-2}). \[ y_j \to y(x) \] Für die Differenz zwischen zwei Punkten kann man schreiben
\begin{equation}
  \label{eq:21}
  y_{j+1} - y_j = \left.\frac{\diff y}{\diff x}\right|_{x= (j + 1/2) d} \cdot d \mpunkt
\end{equation}
Die Masse wird im Folgenden durch eine Linienmassendichte ausgedrückt
\begin{equation}
  \label{eq:22}
  m = \varrho d
\end{equation}
und die Federkonstante durch ein Elastizitätsmodul
\begin{equation}
  \label{eq:23}
  D = \frac{\kappa}{d} \mpunkt
\end{equation}
Für ein endliches $d$ ergibt sich eine Lagrangefunktion der Form
\begin{align}
  L(\{y_n\},\{\dot{y}_n\}) &= \sum_{n=1}^{N+1}  \, T_n - V_n  \notag \\
  &= \sum_{n=1}^{N} \,  \frac{m}{2} \dot{y}_n^2 -\sum_{n=1}^{N+1} \frac{D}{2} (y_n - y_{n-1})^2    \notag \\
  &\eqrel{eq:22}[eq:23]{=} \sum_{n=1}^{N} \,  \frac{\varrho d}{2} \dot{y}_n^2 -\sum_{n=1}^{N+1} \frac{\kappa}{2d} (y_n - y_{n-1})^2   \notag \\
  &\approx \sum_{n=1}^{N} \,  \frac{\varrho d}{2} \left( \frac{\partial y(nd,t)}{\partial t} \right)^2 - \sum_{n=1}^{N+1} \frac{\kappa}{2d} \left( \frac{\partial y(d(n-1/2),t)}{\partial x} \right)^2  d^2  \notag \\ 
  &= \frac{\varrho d}{2} \left[ \sum_{n=1}^{N} \, \left( \frac{\partial y(nd,t)}{\partial t} \right)^2 -\sum_{n=1}^{N+1}  \frac{\kappa}{\varrho} \left( \frac{\partial y(d(n-1/2),t}{\partial x} \right)^2 \right]
  \label{eq:24}
\end{align}
Lassen wir $N \to \infty$ und $ d \to 0$ gehen, so wird aus der
diskreten Summe ein Integral
\begin{equation}
  \label{eq:25}
  d \sum_N \to \int \diff x, 
\end{equation}
was auf eine kontinuierliche Lagrangefunktion führt,
\begin{equation}
  \label{eq:26}
  L \eqrel{eq:24}[eq:25]{=} \int \diff x \underbrace{\frac{\varrho}{2} \left[ \left( \frac{\partial y}{\partial t} \right)^2 - \frac{\kappa}{\varrho} \left( \frac{\partial y}{\partial x} \right)^2 \right]}_{\mathcal{L}\left( \frac{\partial y}{\partial t}, \frac{\partial y}{\partial x} \right)}.
\end{equation}
Man nennt $\mathcal{L}$ die \acct{Lagrangedichte} des kontinuierlichen
Systems. Die Lagrangefunktion folgt aus dem Integral über die
Ortskoordinate $x$. Die wichtige Variable ist das Feld $y(x)$, welches
von dem kontinuierlichen Index $x$ abhängt. Um aus der Lagrangedichte
Bewegungsgleichungen gewinnen zu können, gehen wir wie in der
klassischen Mechanik vor und benutzen die Variation der Wirkung,
\[ y(x,t) \to y(x,t) + \delta y(x,t) \mcomma \] wobei an den Randpunkten $\delta y =0$ gilt.
\begin{align}
  S &=  \int L \diff t = \int  \diff t \int  \diff x \mathcal{L}\left( \frac{\partial y}{\partial t}, \frac{\partial y}{\partial x} \right) \notag  \\
  \delta S&=  \int  \diff t \int  \diff x \left[ \frac{\partial \mathcal{L}}{\partial\left( \frac{\partial y}{\partial t} \right)}  \frac{\partial (\delta y)}{\partial t} +   \frac{\partial \mathcal{L}}{\partial\left( \frac{\partial y}{\partial x} \right)} \frac{\partial (\delta y)}{\partial x}  \right]  \notag \\
  &= \int  \diff t \int  \diff x \left[ -  \frac{\partial }{\partial t} \frac{\partial \mathcal{L}}{\partial\left( \frac{\partial y}{\partial t} \right)} - \frac{\partial }{\partial x} \frac{\partial \mathcal{L}}{\partial\left( \frac{\partial y}{\partial x} \right)} \right]  \cdot \delta y = 0 \mcomma
  \label{eq:27}
\end{align}
woraus die Euler-Lagrange-Gleichungen abgeleitet werden können,
\begin{equation}
  \label{eq:28a}
  \frac{\partial }{\partial t} \frac{\partial \mathcal{L}}{\partial\left( \frac{\partial y}{\partial t} \right)} + \frac{\partial }{\partial x} \frac{\partial \mathcal{L}}{\partial\left( \frac{\partial y}{\partial x} \right)} = 0 \mcomma
\end{equation}
oder mit unserem Beispielsystem \eqref{eq:26}
\begin{equation}
  \label{eq:28b}
 \varrho \frac{\partial^2 y}{\partial t^2} - \kappa \frac{\partial^2 y}{\partial x^2} = 0 \mpunkt
\end{equation}
Wir erhalten also eine Wellengleichung für unsere schwingende
Saite und wir sehen, dass die Lagrangedichte und ihre Variation auf
physikalisch sinnvolle Gleichungen führen.

%%% Local Variables: 
%%% mode: latex
%%% mode: flyspell
%%% ispell-local-dictionary: "de_DE"
%%% TeX-master: "../QFT.tex"
%%% End: