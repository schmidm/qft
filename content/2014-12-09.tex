% Marcel Klett

\section{Formalismus zur Behandlung von Wechselwirkungen}

Die nichtlinearen Terme lassen sich zumeist nicht explizit lösen, daher ist eine störungstheoretische Behandlung notwendig.

\subsection{Zeitentwicklung und Wechselwrikungsbild}

Die Feldoperatoren der freien Felder folgen den Heisenberg-Bewegungsgleichungen
\begin{equation}
  \label{eq:48}
  \ii \hbar \dot{\phi} = [\phi,\nOrd{H}] \mcomma
\end{equation}
sind also im \acct{Heisenbergbild} gegeben. Als Beispiel betrachten wir die Klein-Gordon-Gleichung und berechnen die Heisenberg-Bewegungsgleichung für den Feldoperator $\phi(\bm{r},t)$
\begin{align*}
  [\phi(\bm{r},t),\nOrd{H}] \eqrel{eq:312a}[eq:324b]{=} \sqrt{\frac{2 m c^2}{\hbar}} \int \diff^3 \bm{k} \int \diff^3 \bm{k}' E(\bm{k}) 
  \left\{ [a_{\bm{k}},a_{\bm{k}'}^\dagger] a_{\bm{k}'} f_{\bm{k}}(\bm{r},t) +  
  c_{\bm{k}'}^\dagger [c_{\bm{k}}^\dagger,c_{\bm{k}'}] f_{\bm{k}}^* (\bm{r},t)\right\}.
\end{align*}
Dabei ist $E(\bm{k}) = \hbar c k_0$ und $[a_{\bm{k}},a_{\bm{k}'}^\dagger] = \delta(\bm{k} - \bm{k}')$ sowie $[c^\dagger_{\bm{k}} , c_{\bm{k}'}]= - \delta(\bm{k}- \bm{k}')$ und somit
\begin{align*}
  [\phi(\bm{r},t),\nOrd{H}] &= \sqrt{\frac{2 m c^2}{\hbar}} \hbar \left\{
    \int \diff^3 \bm{k} \, a_{\bm{k}} c k_0 f_{\bm{k}}(\bm{r},t) 
    + c_{\bm{k}}^\dagger (- c k_0 f_{\bm{k}}^*(\bm{r},t))
  \right\}.
\end{align*}
Da die Funktion $f_{\bm{k}} \propto \ee^{-\ii k_\mu x^\mu}$ ist, können wir den Term $c k_0 f_{\bm{k}}(\bm{r},t)$ als Ableitung nach der Zeit interpretieren
\begin{align*}
  c k_0  f_{\bm{k}}(\bm{r},t) = \ii c \partial_{ct} f_{\bm{k}}(\bm{r},t).
\end{align*}
Damit erhalten wir schließlich
\begin{equation}
  [\phi(\bm{r},t),\nOrd{H}] = \ii \hbar \partial_t \phi(\bm{r},t) \mpunkt \label{eq:49}
\end{equation}
Eine formale Lösung für den Feldoperator ist
\begin{equation}
  \label{eq:410}
  \phi_H(\bm{r},t) = \ee^{\ii \nOrd{H} t/\hbar} \phi_S(\bm{r}) \ee^{-\ii \nOrd{H} t/\hbar}\mpunkt
\end{equation}
Dabei symbolisiert der Index $H$, dass es sich um einen Operator im Heisenbergbild handelt und entsprechend kennzeichnet $S$ einen Operator im Schrödingerbild $\phi_S(\bm{r}) = \phi_H(\bm{r},0)$. 

Im Folgenden werden wir eine typische Aufteilung des Hamiltonoperators
\begin{equation}
  \label{eq:411}
  H = H_0 + H_1
\end{equation}
verwenden. Dabei ist $H_0$ der Hamiltonoperator des freien Feldes und $H_1$ beschreibt die Wechselwirkung, welche wir als kleine Störung behandeln wollen. Dazu definieren wir zuerst das \acct{Wechselwirkungsbild} eines Operators $A$ im Schrödingerbild, der eine explizite Zeitabhängigkeit trägt
\begin{equation}
  \label{eq:412}
  A_I(t) =  \ee^{\ii H_0 t/\hbar} A_S(t) \ee^{-\ii H_0 t/\hbar} \mcomma
\end{equation}
was zu einer Heisenberg-Bewegungsgleichung
\begin{subequations}
\begin{align}
  \label{eq:413a}
  \ii \hbar \dot{A}_I(t) &= [A_I(t),H_0] + \ii \hbar \partial_t A_I(t) \\
  \intertext{mit}
  \partial_t A_I = \ee^{\ii H_0 t/\hbar} \partial_t A_S \ee^{- \ii H_0 t/\hbar}    \label{eq:413b}
\end{align}
\end{subequations}
führt. Insebesondere gilt für Feldoperatoren
\begin{equation}
  \label{eq:414}
  \ii \hbar \dot{\phi}_I(\bm{r},t) = [\phi_I(\bm{r},t),H_0] \mpunkt
\end{equation}
Diese besitzen keine explizite Zeitabhängigkeit im Schrödingerbild. Nach \eqref{eq:412} und \eqref{eq:411} entspricht die Wechselwirkungsdarstellung dem Heisenbergbild der freien Felder, also
\begin{equation}
  \label{eq:415}
  \phi_I(\bm{r},t)  = \phi_{H, \text{freie Felder}}.
\end{equation}
Für den Wechselwirkungsterm gilt dann
\begin{equation}
  \label{eq:416}
  H_{1I} =  \ee^{\ii H_0 t/\hbar} H_1(\Phi(\bm{r},t),\ldots) \ee^{-\ii H_0 t/\hbar}  = H_1(\Phi_I(\bm{r},t), \ldots).
\end{equation}
Verwendet man die Heisenbergdarstellung der Feldoperatoren, also zum Beispiel \eqref{eq:312a} und \eqref{eq:312b} für das Klein-Gordon-Feld, erhält man automatisch die Wechselwirkungsdarstellung $H_{1I}$ des Wechselwirkungsterms $H_1$. Damit ist $H_{1I}$ explizit zeitabhängig. 

Die Zeitentwicklung im Wechselwirkungsbild lautet für einen Zustand
\begin{subequations}
  \begin{align}
    \label{eq:417a}
    \Ket{\psi_I(t)} &= \ee^{\ii H_0 t/\hbar} \Ket{\psi_S(t)} \mcomma \\
    \label{eq:417b}
    \ii \hbar \partial_t \Ket{\psi_I(t)} &= H_{1I}(t) \Ket{\psi_I(t)} \mpunkt
  \end{align}
Wie wir sehen, wird die Zeitentwicklung nur durch $H_{1I}$ (im Folgenden nur noch $H_1$ genannt) bestimmt. Der Zeitentwicklungsoperator ergibt sich dann zu
\begin{align}
  \label{eq:417c}
  \Ket{\psi_I(t)} &= U_I(t,t_0) \Ket{\psi_I(t_0)} \mcomma \\
  \label{eq:417d}
  U_I(t,t_0) &= \ee^{\ii H_0 t/\hbar} \ee^{-\ii H_0 (t-t_0)\hbar} \ee^{-\ii H_0 t_0/\hbar} \mpunkt
\end{align}
\end{subequations}
Dabei gilt stets
\begin{equation}
  \label{eq:418}
  \ii \hbar \partial_t U_I(t,t_0) = H_I U_I(t,t_0) \mpunkt
\end{equation}

\subsection{Störungsrechnung}

Die formale Lösung von \eqref{eq:418} ist gegeben durch
\begin{equation}
  \label{eq:419}
  U(t,t_0) = \mathds{1} - \frac{\ii}{\hbar} \int_{t_0}^{t} \diff t' H(t') U(t',t_0) \mpunkt
\end{equation}
Dabei ist der erste Term in \eqref{eq:419} so gewählt, dass die Unitarität von $U(t,t_0)$ gewährleistet ist. Im Rahmen der Störungstheorie benutzen wir einen Iterationsansatz der Form
\begin{subequations}
\begin{equation}
  \label{eq:420a}
  U(t,t_0) = \mathds{1} + \sum_{n=1}^{\infty} U_n(t,t_0) \mpunkt
\end{equation}
Einsetzen von \eqref{eq:420a} in \eqref{eq:419} führt auf die Rekursionsbeziehung 
\begin{equation}
  \label{eq:420b}
  U_n(t,t_0) = - \frac{\ii}{\hbar} \int_{t_0}^{t} \diff t' H_I(t') U_{n-1}(t',t_0) \mpunkt
\end{equation}
\end{subequations}
Damit ergibt sich schließlich
\begin{equation}
  \label{eq:421}
  U_n(t,t_0) \eqrel{eq:420a}[eq:420b]{=} - \frac{\ii}{\hbar} \int_{t_0}^{t} \diff t' H_I(t') - \frac{1}{\hbar^2} \int_{t_0}^{t} \diff t' \int_{t_0}^{t'} \diff t'' H_I(t') H_I(t'') + \ldots
\end{equation}
Mit dem \acct{Zeitordnungsoperator} $T$ kann dieser Term vereinfacht werden. Seine Wirkung ist
\begin{subequations}
\begin{equation}
  \label{eq:422a}
  T (A(t_1) A(t_2) A(t_3) A(t_4)) = (\pm 1)^p A(t_{\alpha_1}) A(t_{\alpha_2}) \ldots
\end{equation}
Für Bosonen wird das positive Vorzeichen benötigt, für Fermionen das negative. Dabei steht $p$ für die Anzahl der Permutationen, die benötigt werden, um ein zeitgeordnetes Produkt zu erzeugen, also
\begin{equation}
  \label{eq:422b}
  t_{\alpha_1} < t_{\alpha_2} < t_{\alpha_3} \ldots 
\end{equation}
\end{subequations}
Für einen Term $n$-ter Ordnung in \eqref{eq:421} lässt sich dann schreiben
\begin{align}
   &\frac{1}{\hbar^n} \int_{t_0}^{t} \diff t_1 \int_{t_0}^{t_1} \diff t_2 \ldots \int_{t_0}^{t_{n-1}} \diff t_n \, H_I(t_1) H_I(t_2) \ldots H_I(t_n) \notag \\
  &\eqrel{eq:422a}[eq:422b]{=} \frac{1}{n! \hbar^n} \int_{t_0}^{t} \diff t_1 \int_{t_0}^{t} \diff t_2 \ldots \int_{t_0}^{t} \diff t_n \, T(H_I(t_1) H_I(t_2) \ldots H_I(t_n)) \label{eq:423}
\end{align}
oder schließlich
\begin{align}
  \notag
  U(t,t_0)  &\eqrel{eq:421}[eq:423]{=} \mathds{1} + \sum_{n=1}^{\infty} \frac{1}{n!} \left(-\frac{\ii}{\hbar} \right)^n \int_{t_0}^{t} \diff t_1 \ldots \int_{t_0}^{t} \diff t_n \, T(H_I(t_1) \ldots H_I(t_n)) \\
  &= T \ee^{-\frac{\ii}{\hbar} \int_{t_0}^{t} \diff t' H_I(t')}. \label{eq:424}
\end{align}
In einer Störungsrechnung bestimmt man nicht die volle Reihe in \eqref{eq:424}, sondern die niedrigsten Ordnungen, also zum Beispiel
\begin{equation}
  \label{eq:425}
  U_1(t,t_0) = - \frac{\ii}{\hbar} \int_{t_0}^{t} H_I(t') \diff t'
\end{equation}
für die erste Ordnung, was dann auf einen genäherten Zeitentwicklungsoperator der Form
\begin{equation}
  \label{eq:426}
  U(t,t_0) \approx \mathds{1} + U_1(t,t_0)
\end{equation}
führt.

\subsection{Die Streumatrix}

Wir wollen im Folgenden Vorgänge betrachten, die wie folgt ablaufen. Am Anfang ($t \to -\infty)$ liegt die Situation vor, dass sich ein freies Teilchen dem Streuzentrum nähert, gestreut wird und zu einem Zeitpunkt $t \to \infty$ wieder als freies Teilchen behandelt werden kann (siehe auch Bild \ref{fig:2014-12-09-1}).
\begin{figure}[tb]
  \centering
  \begin{tikzpicture}[gfx]
    \node[draw,circle,minimum size=2cm] (W) at (0,0) {};
    \node[below=of W] {Wechselwirkungsbereich};
    \node[text width=3cm,align=center,left=of W] (L) {%
      Elementarteilchen bewegen sich aufeinander zu};
    \node[text width=3cm,align=center,right=of W] (R) {%
      Elementarteilchen entfernen sich voneinander};
    \draw[->] (L.north east) -- (W);
    \draw[->] (L.south east) -- (W);
    \draw[<-] (R.north west) -- (W);
    \draw[<-] (R.south west) -- (W);
  \end{tikzpicture}
  \caption{Einfallende Teilchen verhalten sich für Zeiten $t \to - \infty$ wie freie Teilchen. Nachdem sie den Wechselwirkunsgbereich durchlaufen haben, sind sie für Zeiten $t \to \infty$ wieder als freie Teilchen zu interpretieren.}
  \label{fig:2014-12-09-1}
\end{figure}
Für zwei Teilchen haben wir anfänglich den Zustand
\begin{equation}
  \label{eq:427}
  \Ket{i} = \Ket{\bm{k}_1,S_1} \Ket{\bm{k}_2,S_2} = \frac{1}{\sqrt{2}} a^\dagger(\bm{k}_1,S_1) a^\dagger(\bm{k}_2,S_2) \Ket{0},
\end{equation}
wobei der Zustand $\Ket{0}$ den Vakuumszustand beschreibt. Unter der Annahme, dass der freie Zustand $\ket{i}$ zur Zeit $t \to -\infty$ vorliegt, erhalten wir für die Zeitentwicklung
\begin{equation}
  \label{eq:428}
  \Ket{\psi_i(t)} = U(t,-\infty) \Ket{i} \mpunkt
\end{equation}
Zur Zeit $t \to \infty$ liegt der Endzustand $\ket{f}$ vor, der sich erneut durch freie Teilchenzustände beschreiben lässt,
\begin{subequations}
  \begin{align}
    \Ket{f} &= U(\infty,t) \ket{\psi_f(t)} \label{eq:429a} \mcomma \\
    \ket{\psi_f(t)} &= U(t,\infty) \Ket{f} \label{eq:429b} \mpunkt
  \end{align}
\end{subequations}
Es folgt für die Wahrscheinlichkeit, dass der Übergang $\Ket{i} \to \ket{f}$ stattfindet
\begin{equation}
  \label{eq:430}
  \Braket{\psi_f(t)|\psi_i(t)} = \Braket{f|U(\infty,-\infty)|i} \equiv S_{fi} \mcomma
\end{equation}
mit dem Element $S_{fi}$ der \acct{Streumatrix} $S$. Daraus folgern wir
\begin{equation}
  \label{eq:431}
  S = U(\infty,-\infty) \eqrel{eq:424}{=} T \, \ee^{-\frac{\ii}{\hbar} \int_{-\infty}^{\infty} \diff t' H_I(t')}.
\end{equation}

\paragraph{Vorgehen:}

Um die Streumatrix $S$ bestimmen zu können, entwicklen wir $S$ in eine Störungsreihe. Dies ist jedoch schwer auszuwerten, da wir gesehen haben, dass normalgeordnete Produkte relevant sind, der Zeitordnungsoperator $T$ stört jedoch diese Normalordnung. Ein zeitgeordnetes Produkt lässt sich jedoch in ein normalgeordnetes umschreiben.