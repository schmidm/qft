% Marcel Klett

\chapter{Quantenelektrodynamik}

\section{Wechselwirkungsterm}

\subsection{Streumatrix}

Die Quantenelektrodynamik behandelt die Wechselwirkung von Teilchen
der Dirac-Gleichung (Elektronen, Positronen) mit dem Maxwellfeld
(Photonen). Ausgangspunkt ist hierbei die
Lagrangedichte~\eqref{eq:44}, insbesondere der Wechselwirkungsterm
\begin{align}
    \mathcal{L}_I &= -ce \nOrd{\overline{\psi}\gamma^\mu \underline{\psi} A_\mu}
    = - c e \nOrd{\overline{\psi} \slashed{A} \underline\psi}
  \label{eq:51}
\end{align}
mit den Feldoperatoren 
\begin{align*}
  \underline\psi &= \sum_{s=\uparrow,\downarrow} \int \frac{\diff^3 k}{\sqrt{2\pi}^3} \sqrt{\frac{mc^2}{E_{\bm{k}}}} \left\{ b(\bm{k},s) \underline{u}(\bm{k},s) \ee^{-\ii k_\mu x^\mu} + d^\dagger \underline{v}(\bm{k},s) \ee^{\ii k_\mu x^\mu}  \right\} \mcomma \\
  A^\mu &= \sqrt{\frac{\hbar \mu_0 c^2}{(2\pi)^3}} \sum_\lambda \int \diff^3 k \frac{\varepsilon^\mu(\bm{k},\lambda)}{\sqrt{2 \omega_{\bm{k}}}} \left\{ a(\bm{k},\lambda) \ee^{-\ii k_\mu x^\mu} + a^\dagger(\bm{k},\lambda) \ee^{\ii k_\mu x^\mu} \right\} 
\end{align*}
und der Ladung $q=e=-e_0 < 0$. Wir finden also für den
Hamiltonoperator der Wechselwirkung
\begin{equation}
  \label{eq:52}
  \nOrd{H_I} = \int \nOrd{\mathcal{H}_I} \diff^3 r = c e \int \nOrd{\overline\psi \slashed{A} \underline\psi} \diff^3 r \mpunkt 
\end{equation}
Damit lautet die Streumatrix der QED
\begin{align}
  \notag
  S &= T \ee^{-\ii \int_{-\infty}^{\infty} \nOrd{H_I} \diff t/\hbar } \\
  &= T \ee^{-\ii \int \nOrd{\mathcal{H}_I} \diff^4 x /(c\hbar)} \notag \\
  &\eqrel{eq:52}{=} T \ee^{-\ii e \int \nOrd{\overline\psi \slashed{A} \underline\psi} \diff^4 x / \hbar} \notag \\
  &= \sum_{n=0}^{\infty} \frac{1}{n!} \left( - \frac{\ii e}{\hbar} \right)^n T \prod_{i=1}^{n} \int \diff^4 x_i \, \nOrd{\overline\psi(x_i) \slashed{A}(x_i) \underline\psi(x_i)} \label{eq:53}
\end{align}
sowie die Terme erster und zweiter Ordnung
\begin{subequations}
  \begin{align}
    S^{(1)} &= - \frac{\ii e}{\hbar} \int \diff^4 x \, \nOrd{\overline\psi(x) \slashed{A}(x) \underline\psi(x)}  \label{eq:54a} \mcomma \\
    S^{(2)} &= \frac{1}{2}  \left( - \frac{\ii e}{\hbar} \right)^2 T \int \diff^4 x_1 \diff^4 x_2 \, \nOrd{\overline\psi(x_1) \slashed{A}(x_1) \underline\psi(x_1)} \nOrd{\overline\psi(x_2) \slashed{A}(x_2) \underline\psi(x_2)} \label{eq:54b} \mpunkt
  \end{align}
\end{subequations}

\subsection{Rechtfertigung der Störungsreihe}

Für jede Ordnung ergibt sich in grober Näherung
\begin{equation}
  \label{eq:55}
  e/\hbar \int \diff^4 x \, \nOrd{\overline\psi(x) \slashed{A}(x) \underline\psi(x)} \approx \sqrt{\alpha}
\end{equation}
mit der Feinstrukturkonstanten $\alpha \approx \frac{1}{137}$. Wie wir
später sehen werden, trägt nur jede zweite Ordnung zu den
Übergangsamplituden bei, sodass die Störungsreihe~\eqref{eq:53} einer
Entwicklung nach der kleinen Zahl $\frac{1}{137}$ entspricht.

\section{Feynman-Diagramme}

Die Aufgabe der QED besteht nun darin, die einzelnen Ordnungen der
Ströungsreihe aus~\eqref{eq:53} auszuwerten, um damit physikalische
Prozesse zu erklären. Dabei sind aufgrund von $3 n$ Feldoperatoren in
$n$-ter Ordnung, $2$ Erzeugern oder Vernichtern in den
Modenentwicklungen~\eqref{eq:342a} und~\eqref{eq:366a} und der
Zeitordnung viele Terme zu erwarten. Bei deren Sortierung ist eine
graphische Darstellung in Feynmandiagrammen hilfreich.

Die typische Auftragung erfolgt in Raum-Zeit-Diagrammen,
\begin{center}
  \begin{tikzpicture}
    \draw[->] (0,-0.2) -- (0,2) node[left] {$t$};
    \draw[->] (-0.2,0) -- (2,0) node[below] {$\bm{r}$};
  \end{tikzpicture}
\end{center}
wobei diese Achsen eigentlich nie gezeichnet werden.

In den Feynman-Diagrammen treten folgende Symbole auf:
\begin{itemize}
\item Elektronen als durchgezogene Linien mit Pfeil in positiver Zeitrichtung:
  \begin{center}
    \begin{tikzpicture}
      \matrix[feyn] (m) {
        |(a)| & |(b)| & |(c)| \\
              & |(d)| &       \\
      };
      \draw[fermion] (d) to (a);
      \draw[fermion] (d) to (b);
      \draw[fermion] (d) to (c);
    \end{tikzpicture}
  \end{center}

\item Positronen als durchgezogene Linien, allerdings mit Pfeil in
  negativer Zeitrichtung in Anlehnung an die Feynman-Stückelberg
  Interpretation:
  \begin{center}
    \begin{tikzpicture}
      \matrix[feyn] (m) {
        |(a)| & |(b)| & |(c)| \\
              & |(d)| &       \\
      };
      \draw[fermion] (a) to (d);
      \draw[fermion] (b) to (d);
      \draw[fermion] (c) to (d);
    \end{tikzpicture}
  \end{center}

\item Photonen als gewellte Linien (ohne Pfeil, da es keine
  unabhängigen Antiteilchen gibt):
  \begin{center}
    \begin{tikzpicture}
      \matrix[feyn] (m) {
        |(a)| & |(b)| & |(c)| \\
              & |(d)| &       \\
      };
      \draw[photon] (a) to (d);
      \draw[photon] (b) to (d);
      \draw[photon] (c) to (d);
    \end{tikzpicture}
  \end{center}
\item Externe statische (waagrecht im Diagramm) Felder sind durch
  Wellenlinien mit einem Kreuz am Ende gekennzeichnet:
  \begin{center}
    \begin{tikzpicture}
      \matrix[feyn] (m) {
        |[cross](a)| & & |(b)| \\
      };
      \draw[photon] (a) to (b);
    \end{tikzpicture}
  \end{center}

\item Das Erzeugen und Vernichten von Teilchen, also das Auftreten der
  Operatoren $a$, $a^\dagger$, $b$, $b^\dagger$, $d$, $d^\dagger$ wird
  durch einen Punkt (Vertex) gekennzeichnet: \\
  \begin{center}
    \begin{tikzpicture}
      \draw[fill,->] circle (0.075);
    \end{tikzpicture}
  \end{center}

\end{itemize}
Mit diesen elementaren Bausteinen kann man folgende Situationen
beschreiben:
\begin{itemize}
\item Ein- und auslaufendes Teilchen, also freie Teilchen bei $t = \pm
  \infty$ haben nur auf einer Seite einen Vertex. Zum Beispiel ein
  einlaufendes Elektron und auslaufendes Positron
  \begin{equation*}
    \underbrace{\text{
        \begin{tikzpicture}
          \matrix[feyn,ampersand replacement=\&] (m) {
                  \& \& |[vertex](b)| \\
            |(d)| \& \&               \\
          };
          \draw[fermion] (d) to (b);
          \node[draw,circle,MidnightBlue,pin={[pin distance=.1cm]below:Anfangzustand $\ket{i}$}] at (d) {};
          \node[draw,circle,MidnightBlue,pin={[pin distance=.1cm]above:Vernichter $b$ des Elektrons}] at (b) {};
        \end{tikzpicture}
      }}_{b\ket{i}}
    \qquad
    \underbrace{\text{
        \begin{tikzpicture}
          \matrix[feyn,ampersand replacement=\&] (m) {
                          \&  \& |(d)| \\
            |[vertex](b)| \&  \&       \\
          };
          \draw[photon] (d) to (b);
          \node[draw,circle,MidnightBlue,pin={[pin distance=.1cm]below:Photonenerzeugung $a^\dagger$}] at (b) {};
          \node[draw,circle,MidnightBlue,pin={[pin distance=.1cm]above:Endzustand $\ket{f}$}] at (d) {};
        \end{tikzpicture}
      }}_{\bra{f}a^\dagger}
  \end{equation*}
\item Sogennante innere Linien, Linien mit zwei Vertizes beschreiben
  ein Teilchen, das in einem Prozess erzeugt und anschließend in einem
  weiteren vernichtet wird
  \begin{center}
    \begin{tikzpicture}
      \matrix[feyn] (m) {
                      & |[vertex](d)| \\
        |[vertex](b)| &               \\
      };
      \draw[fermion] (d) to (b);
      \node[draw,circle,MidnightBlue,pin={[pin distance=.1cm]right:Erzeuger eines Positrons $d^\dagger$}] at (b) {};
      \node[draw,circle,MidnightBlue,pin={[pin distance=.1cm]right:Vernichter eines Positrons $d$}] at (d) {};
    \end{tikzpicture}
  \end{center}
  Innere Linien beschreiben Teilchen, welche nach außen nicht
  auftreten können -- sie werden virtuelle Teilchen gennant. Sie
  treten in Kontraktionen auf, zum Beispiel
  \begin{equation*}
    \underbracket{\underline\psi \overline\psi} \eqrel{eq:449}{=} \ii S_F \mpunkt
  \end{equation*}
  Diese enthalten immer die Propagationen von Teilchen und
  Antiteilchen, daher stellt man sie im Diagramm ohne Zeitrichtung
  dar.
  \begin{equation*}
    \begin{tikzpicture}[baseline=(m.center)]
      \matrix[feyn] (m) {
        |[vertex](b)| & & |[vertex](d)| \\
      };
      \draw[fermion] (b) to (d);
    \end{tikzpicture}
    \mathrel{% align \equiv to the axis of (m.center)
      \lower\dimexpr\fontdimen22\textfont2\relax\hbox{$\equiv$}
    }
    \begin{tikzpicture}[baseline=(m.center)]
      \matrix[feyn] (m) {
                      & |[vertex](a)| \\   
        |[vertex](b)| &               \\ 
        |[vertex](c)| &               \\
                      & |[vertex](d)| \\  
      };
      \draw[fermion] (b) to (a);
      \draw[fermion] (c) to (d);
    \end{tikzpicture}
  \end{equation*}
\end{itemize}

\section{Ein Prozess erster Ordnung}

\subsection{Beitrag zur Streumatrix}

Im Folgenden wollen wir die erste Ordnung der Streumatrix
\begin{equation*}
  S^{(1)} \eqrel{eq:54a}{=} - \frac{\ii e}{\hbar} \int \diff^4 x \, \nOrd{\overline\psi(x) \slashed{A}(x) \underline\psi(x)}  
\end{equation*}
näher betrachten. Dazu definieren wir zunächst folgende Abkürzungen:
\begin{subequations}
  \begin{align}
    \underline{\psi}_b &= \sum_{s= \uparrow, \downarrow} \int \frac{\diff^3 k}{\sqrt{2\pi}^3} \sqrt{\frac{mc^2}{E_{\bm{k}}}}
    b(\bm{k},s) \, \underline{u}(\bm{k},s) \ee^{- \ii k_\mu x^\mu}
    \label{eq:56a}\\
    \overline{\psi}_{b^\dagger} &= \sum_{s= \uparrow, \downarrow} \int \frac{\diff^3 k}{\sqrt{2\pi}^3} \sqrt{\frac{mc^2}{E_{\bm{k}}}}
    b^\dagger(\bm{k},s) \, \overline{u}(\bm{k},s) \ee^{\ii k_\mu x^\mu}
    \label{eq:56b}\\
    \underline{\psi}_{d^\dagger} &= \sum_{s= \uparrow, \downarrow}\int \frac{\diff^3 k}{\sqrt{2\pi}^3} \sqrt{\frac{mc^2}{E_{\bm{k}}}}
    d^\dagger(\bm{k},s) \, \underline{v}(\bm{k},s) \ee^{\ii k_\mu x^\mu}
    \label{eq:56c}\\
    \overline{\psi}_{d} &= \sum_{s= \uparrow, \downarrow}\int \frac{\diff^3 k}{\sqrt{2\pi}^3} \sqrt{\frac{mc^2}{E_{\bm{k}}}}
    d(\bm{k},s) \, \overline{v}(\bm{k},s) \ee^{- \ii k_\mu x^\mu}
    \label{eq:56d}\\
    A_{a\alpha} &= \sqrt{\frac{\hbar \mu_0 c^2}{(2\pi)^3}} \sum_\lambda \int \diff^3 k \frac{\varepsilon_\alpha(\bm{k},\lambda)}{\sqrt{2 \omega_{\bm{k}}}}
    a(\bm{k},\lambda) \ee^{-\ii k_\mu x^\mu}
    \label{eq:56e}\\
    A_{a^\dagger \alpha} &= \sqrt{\frac{\hbar \mu_0 c^2}{(2\pi)^3}} \sum_\lambda \int \diff^3 k \frac{\varepsilon_\alpha(\bm{k},\lambda)}{\sqrt{2 \omega_{\bm{k}}}}
    a^\dagger(\bm{k},\lambda) \ee^{\ii k_\mu x^\mu}
    \label{eq:56f}
  \end{align}
\end{subequations}
Alle Operatoren treten in~\eqref{eq:54a} am selben Raum-Zeit-Punkt
auf, das heißt es gibt nur einen Vertex. Allgemein gilt, dass in der
Ordnung $n$ aus~\eqref{eq:53} $n$ Integrationsvariablen $x^\mu$
vorkommen und damit $n$ Vertizes.  Damit findet man
\begin{align}
  S^{(1)} \eqrel{eq:54a}{=}
  \notag &
  -(\ii e/\hbar) \int \overline{\psi}_{b^\dagger} \gamma^\alpha A_{a\alpha} \underline{\psi}_b \diff^4 x
  &
  \begin{tikzpicture}[baseline={([yshift=\dimexpr-\fontdimen22\textfont2\relax]m.center)}]
    \matrix[feyn,ampersand replacement=\&] (m) {
            \&               \& |(b)| \\
            \& |[vertex](c)| \&       \\
      |(d)| \&               \& |(a)| \\
    };
    \draw[fermion] (d) node[left]{$e^-$} to (c);
    \draw[photon]  (c) to (a) node [right]{$\gamma$};
    \draw[fermion] (c) to (b) node[right]{$e^-$};
  \end{tikzpicture}
  \\
  \notag &
  -(\ii e/\hbar) \int \overline{\psi}_{b^\dagger} \gamma^\alpha  \underline{\psi}_{d^\dagger} A_{a\alpha} \diff^4 x
  &
  \begin{tikzpicture}[baseline={([yshift=\dimexpr-\fontdimen22\textfont2\relax]m.center)}]
    \matrix[feyn,ampersand replacement=\&] (m) {
      |(a)| \&               \& |(b)| \\
            \& |[vertex](c)| \&       \\
            \& |(d)|         \&       \\
    };
    \draw[photon]  (d) node[left] {$\gamma$} to (c);
    \draw[fermion] (a) node[left] {$e^+$} to (c);
    \draw[fermion] (c) to (b) node [right] {$e^-$};
  \end{tikzpicture}
  \\
  \notag &
  -(\ii e/\hbar) \int \overline{\psi}_{b^\dagger} \gamma^\alpha A_{a^\dagger\alpha} \underline{\psi}_{b} \diff^4 x
  &
  \begin{tikzpicture}[baseline={([yshift=\dimexpr-\fontdimen22\textfont2\relax]m.center)}]
    \matrix[feyn,ampersand replacement=\&] (m) {
      |(a)| \&               \& |(b)| \\
            \& |[vertex](c)| \&       \\
      |(d)| \&               \&       \\
    };
    \draw[fermion] (d) node [right] {$e^-$} to (c);
    \draw[photon]  (a) node[left] {$\gamma$} to (c);
    \draw[fermion] (c) to (b) node [right] {$e^-$};
  \end{tikzpicture}
  \\
  \notag &
  -(\ii e/\hbar) \int \overline{\psi}_{b^\dagger} \gamma^\alpha A_{a^\dagger\alpha} \underline{\psi}_{d^\dagger} \diff^4 x
  &
  \begin{tikzpicture}[baseline={([yshift=\dimexpr-\fontdimen22\textfont2\relax]m.center)}]
    \matrix[feyn,ampersand replacement=\&={.7,.5}] (m) {
      |(a)| \& |(b)|         \& |(c)| \\
            \& |[vertex](d)| \&       \\
    };
    \draw[photon]  (b) node[right]{$\gamma$} to (d);
    \draw[fermion] (a) node[left] {$e^+$} to (d);
    \draw[fermion] (d) to (c) node[right]{$e^-$};
  \end{tikzpicture}
  \\
  \notag &
  -(\ii e/\hbar) \int \overline{\psi}_{d} \gamma^\alpha A_{a\alpha} \underline{\psi}_{b} \diff^4 x
  &
  \begin{tikzpicture}[baseline={([yshift=\dimexpr-\fontdimen22\textfont2\relax]m.center)}]
    \matrix[feyn,ampersand replacement=\&={.7,.5}] (m) {
            \& |[vertex](d)| \&       \\
      |(a)| \& |(b)|         \& |(c)| \\
    };
    \draw[photon]  (b) node[below] {$\gamma$} to (d);
    \draw[fermion] (a) node[left] {$e^-$}to (d);
    \draw[fermion] (d) to (c) node[right]{$e^+$};
  \end{tikzpicture}
  \\
  \notag &
  -(\ii e/\hbar) \int \, \nOrd{\overline{\psi}_{d} \gamma^\alpha  \underline{\psi}_{d^\dagger}} \,A_{a\alpha} \diff^4 x
  &
  \begin{tikzpicture}[baseline={([yshift=\dimexpr-\fontdimen22\textfont2\relax]m.center)}]
    \matrix[feyn,ampersand replacement=\&] (m) {
            \&               \& |(b)| \\
            \& |[vertex](c)| \&       \\
      |(d)| \&               \& |(a)| \\
    };
    \draw[fermion] (d) node[left] {$e^-$}to (c);
    \draw[photon]  (a) node[right] {$\gamma$} to (c);
    \draw[fermion] (c) to (b) node[right] {$e^-$};
  \end{tikzpicture}
  \\
  \notag &
  -(\ii e/\hbar) \int A_{a^\dagger\alpha} \overline{\psi}_{d} \gamma^\alpha  \underline{\psi}_{b} \diff^4 x
  &
  \begin{tikzpicture}[baseline={([yshift=\dimexpr-\fontdimen22\textfont2\relax]m.center)}]
    \matrix[feyn,ampersand replacement=\&] (m) {
            \& |(d)|         \&       \\
            \& |[vertex](c)| \&       \\
      |(a)| \&               \& |(b)| \\
    };
    \draw[photon]  (d) node[right] {$\gamma$} to (c);
    \draw[fermion] (a) node[left]{$e^-$}to (c);
    \draw[fermion] (c) to (b) node[right]{$e^+$};
  \end{tikzpicture}
  \\
  &
  -(\ii e/\hbar) \int A_{a^\dagger\alpha} \, \nOrd{\overline{\psi}_{d} \gamma^\alpha \underline{\psi}_{d^\dagger}} \, \diff^4 x
  &
  \begin{tikzpicture}[baseline={([yshift=\dimexpr-\fontdimen22\textfont2\relax]m.center)}]
    \matrix[feyn,ampersand replacement=\&] (m) {
      |(a)| \&               \& |(b)| \\
            \& |[vertex](c)| \&       \\
      |(d)| \&               \&       \\
    };
    \draw[fermion] (c) to (d) node[right]{$e^+$};
    \draw[photon]  (a) node[left] {$\gamma$} to (c);
    \draw[fermion] (b) node[right] {$e^+$}to (c);
  \end{tikzpicture}
  \label{eq:57}
\end{align}
