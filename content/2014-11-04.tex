% Marcel Klett

\section{Lagrangeformalismus für allgemeine Felder}

Wir führen eine Lagrangedichte ein, die von mehreren Feldern
$\phi_r(x)$ und allen ihren ersten Ableitungen $\phi_{r,\mu}
= \partial_\mu \phi_r(x)$ abhängt.
\begin{equation}
  \label{eq:29}
  \mathcal{L} = \mathcal{L}(\phi_r,\phi_{r,\mu}) = \mathcal{L}(\phi, \partial x^0 \phi, \partial x^1 \phi_1, \partial x^2 \phi_1,\partial x^3 \phi_1, \partial x^0 \phi_2, \ldots)
\end{equation}
Die Lagrangefunktion ergibt sich dann aus der Integration \acct*{nur} über die Ortskoordinaten,
\begin{equation}
  \label{eq:210}
  L(t) = \int \diff^3 \bm{r} \, \mathcal{L} (\phi_r , \phi_{r,\mu}) \mpunkt
\end{equation}
Das Integral über das Gebiet $\Omega$ der Raumzeit ergibt dann die
Wirkung
\begin{equation}
  \label{eq:211}
  S(\Omega) = \int_\Omega \diff^4 x \, \mathcal{L} (\phi_r , \phi_{r,\mu}) \mpunkt
\end{equation}

\subsubsection{Hamiltonsches Variationsprinzip}

Analog des Beispiels der schwingenden Saite erfolgt die Variation der
Felder
\begin{equation}
  \label{eq:212a}
  \phi_r(x) \to \phi_r(x) + \delta \phi_r(x) = \tilde{\phi}_r 
\end{equation}
derart, dass 
\begin{equation}
  \label{eq:212b}
  \delta \phi_r(x)\big|_{\partial \Omega} = 0 
\end{equation}
gilt. Eine Variation der Wirkung \eqref{eq:211} führt auf die Form
\begin{equation}
  \label{eq:213a}
  \delta S = \int_\Omega \diff^4 x \, \left[ \frac{\partial
      \mathcal{L}}{\partial \phi_r} \delta \phi_r + \frac{\partial
      \mathcal{L}}{\partial \phi_{r,\mu}} \delta \phi_{r,\mu} \right] \mcomma
\end{equation}
wobei folgende Relation verwendet wurde
\begin{align}
  \delta \phi_r(x) &\eqrel{eq:212a}{=} \tilde{\phi}_r(x) - \phi_r(x) \mcomma \notag \\
  \partial_\mu \phi_r(x) &= \tilde{\phi}_{r,\mu}(x) - \phi_{r,\mu}(x) = \delta \phi_{r,\mu}(x) \mcomma  \notag \\
  \leadsto \delta \phi_{r,\mu} &= \delta (\partial_\mu \phi_r(x)) = \partial_\mu \delta \phi_r(x) \mpunkt
  \label{eq:213b}
\end{align}
Weiterhin verwendet man
\begin{equation}
  \label{eq:213c}
  \frac{\partial \mathcal{L}}{\partial \phi_{r, \mu}} \delta \phi_{r,\mu} = \partial_\mu
  \left( \frac{\partial \mathcal{L}}{\partial \phi_{r,\mu}} \delta
    \phi_r \right) - \left( \partial_\mu \frac{\partial
      \mathcal{L}}{\partial \phi_{r,\mu}} \right) \delta \phi_r \mpunkt
\end{equation}
Damit lässt sich die Variation der Wirkung schreiben als
\begin{equation}
  \label{eq:214a}
  \delta S \eqrel{eq:213a}[eq:213c]{=} \int_\Omega \diff^4 x \,
  \left\{ \left[ \frac{\partial \mathcal{L}}{\partial \phi_r}
      - \partial_\mu \frac{\partial \mathcal{L}}{\partial \phi_{r,\mu}}
    \right] \delta \phi_r + \partial_\mu \left[ \frac{\partial
        \mathcal{L}}{\partial \phi_{r,\mu}} \delta \phi_r \right]\right\} \mpunkt
\end{equation}
Mit dem Gaußschen Satz berechnet man schließlich
\begin{equation}
  \label{eq:214b}
  \int_\Omega \diff^4 x \, \partial_\mu \left[ \frac{\partial
      \mathcal{L}}{\partial \phi_{r,\mu}} \delta \phi_r \right] =
  \int_{\partial \Omega} \diff \sigma_\mu \, \frac{\partial
    \mathcal{L}}{\partial \phi_{r,\mu}} \delta \phi_r = 0.
\end{equation}
Daraus folgt für \eqref{eq:214a}
\begin{equation}
  \label{eq:215}
  \delta S = \int_\Omega \diff^4 x \, \left\{ \frac{\partial
      \mathcal{L}}{\partial \phi_r} - \partial_\mu \frac{\partial
      \mathcal{L}}{\partial \phi_{r,\mu}} \right\} \delta \phi_r.
\end{equation}
Da diese Gleichung unabhängig von $\delta \phi_r$ erfüllt sein soll,
folgen die Euler-Lagrangegleichungen für die Felder.
\begin{equation}
  \label{eq:216}
  \frac{\partial \mathcal{L}}{\partial \phi_r} - \partial_\mu
  \frac{\partial \mathcal{L}}{\partial \phi_{r,\mu}} = 0.
\end{equation}
Den Aufwand, den wir betrieben haben, um eine Euler-Lagrangegleichung
für Felder herzuleiten, wird dann nützlich, wenn wir den Formalismus
auf relativistische Felder anwenden.

\subsubsection{Hamiltondichte}

Analog zur klassischen Punktmechanik führt man über
\[ \dot{\phi}_r = \phi_{r,t} \] die Impulsdichte $\pi_r$ ein
\begin{equation}
  \label{eq:217}
  \pi_r = \frac{\mathcal{L}}{\partial \dot{\phi}_r} = \frac{\partial
    \mathcal{L}}{\partial \phi_{r,t}}.
\end{equation}
Die Impulsdichte hängt dabei im Allgemeinen von allen
Raum-Zeit-Ableitungen als auch vom Ort selbst ab
\begin{equation}
  \label{eq_2014-11-4-1}
  \pi_r = \pi_r (\dot{\phi}_r, \nabla\phi_r  , \phi_r, x).
\end{equation}
Wir gehen davon aus, dass \eqref{eq_2014-11-4-1} invertierbar ist,
sodass wir
\begin{equation}
  \label{eq:2014-11-4-2}
  \dot{\phi}_r = \dot{\phi}_r  (\pi_r, \nabla \phi_r , \phi_r ,x)
\end{equation}
erhalten. Aus diesen Relationen lässt sich eine \acct{Hamiltondichte}
$\mathcal{H}$ formulieren,
\begin{equation}
  \label{eq:219}
  \mathcal{H} = \sum_r \pi_r \dot{\phi}_r - \mathcal{L} (\phi_r ,
  \dot{\phi}_r) = \dot{\phi}_r \left( \pi_r, \nabla \phi_r , \phi_r ,x \right) ) \mpunkt
\end{equation}
Um aus der Hamiltondichte Bewegungsgleichungen zu erhalten, berechnen
wir
\begin{align*}
  \frac{\partial \mathcal{H}}{\partial \pi_r} &= \dot{\phi}_r + \sum_s
  \pi_s \frac{\partial \dot{\phi}_s}{\partial \pi_r} - \sum_s
  \frac{\partial \mathcal{L}}{\partial \dot{\phi}_s} \frac{\partial
    \dot{\phi}_s}{\partial \pi_r} \\
  &= \dot{\phi}_r + \underbrace{\sum_s \left( \pi_s - \frac{\partial
        \mathcal{L}}{\partial \dot{\phi}_s} \right)}_{=0} \frac{\partial
    \dot{\phi}_s}{\partial \pi_r}
\end{align*}
oder
\begin{equation}
  \label{eq:221}
  \dot{\phi}_r = \frac{\partial \mathcal{H}}{\partial \pi_r}  \mpunkt
\end{equation}
Weiterhin wollen wir die Hamiltondichte nach den Feldern ableiten und
erhalten
\begin{align}
  \frac{\partial \mathcal{H}}{\partial \phi_r} &\eqrel{eq:219}{=}
  \sum_s \pi_s \frac{\partial \dot{\phi}_s}{\partial \phi_r} -
  \frac{\partial \mathcal{L}}{\partial \phi_r} - \sum_s \frac{\partial
    \mathcal{L}}{\partial \dot{\phi}_s} \frac{\partial \dot{\phi}_s}{\partial
    \phi_r} \notag \\
  &\eqrel{eq:217}{=} - \frac{\partial \mathcal{L}}{\partial \phi_r}
  \eqrel{eq:216}{=} - \partial_\mu  \frac{\partial \mathcal{L}}{\partial \phi_{r,\mu}} \notag \\
  &= - \frac{\partial}{\partial (ct)} \frac{\partial \mathcal{L}}{\partial \phi_{r,ct}}
  - \frac{\partial}{\partial x^l} \frac{\partial \mathcal{L}}{\partial \phi_{r,l}} \notag \\
  &= - \partial_t \frac{\partial \mathcal{L}}{\partial \dot{\phi}_{r}}
  - \frac{\partial}{\partial x^l} \frac{\partial \mathcal{L}}{\partial \phi_{r,l}} \notag \\ 
  &\eqrel{eq:217}{=} - \dot{\pi}_r -  \frac{\partial}{\partial x^l} \frac{\partial \mathcal{L}}{\partial \phi_{r,l}} \mcomma
  \label{eq:222a}
\end{align}
wobei sich der letzte Term in \eqref{eq:222a} noch wie folgt umschreiben lässt.
\begin{equation}
  \label{eq:222b}
  \frac{\partial \mathcal{H}}{\partial \phi_{r,l}} = \sum_r \pi_r
  \frac{\partial \dot{\phi}_r}{\partial \phi_{r,l}} - \frac{\partial
    \mathcal{L}}{\partial \phi_{r,l}} - \sum_s \frac{\partial
    \mathcal{L}}{\partial \dot{\phi}_s} \frac{\partial
    \dot{\phi}_s}{\partial \phi_{r,l}} \stackrel{\eqref{eq:217}}{=} -
  \frac{\partial \mathcal{L}}{\partial \phi_{r,l}}.
\end{equation}
Wir erhalten damit für die zeitliche Ableitung der Impulsdichte
\begin{equation}
  \label{eq:223}
  \dot{\pi}_r \eqrel{eq:222a}[eq:222b]{=} - \frac{\partial
    \mathcal{H}}{\partial \phi_r} + \frac{\partial}{\partial x^l}
  \frac{\partial \mathcal{H}}{\partial \phi_{r,l}}.
\end{equation}
Zusammengefasst ergeben sich die \acct{Hamiltonschen Bewegungsgleichungen}
erster Ordnung für die Felder, welche wie folgt lauten
\begin{equation}
  \boxed{
    \dot{\pi}_r = - \frac{\partial \mathcal{H}}{\partial \phi_r} +
    \frac{\partial}{\partial x^l} \frac{\partial \mathcal{H}}{\partial
      \phi_{r,l}} \mcomma   \dot{\phi}_r = \frac{\partial \mathcal{H}}{\partial \pi_r} 
  }
\end{equation}

\section{Feldquantisierung}

\subsection{Zurück zum mechanischen Beispiel}

Wir teilen die Saite in diskrete Massepunkte auf, vergleiche Abbildung~\ref{fig:2014-11-04}.
\begin{figure}[tb]
  \centering
  \begin{tikzpicture}[gfx]
    \draw[->] (-.1,0) -- (5,0);
    \draw[->] (0,-.1) -- (0,2);
    \foreach \i in {1,2,3,4} {
      \draw[dashed] (\i,0) -- (\i,2);
    }
    \node[MidnightBlue,dot] (B) at (2.5,1.5) {};
    \node[MidnightBlue,dot] (A) at (1.5,0.8) {};
    \node[MidnightBlue,dot] (C) at (3.5,1.1) {};
    \draw[MidnightBlue] (A) -- (B) -- (C);
    \node[MidnightBlue,above] at (B) {$y_i$};
    \draw[<->] (2,-.1) -- node[below] {$\Delta x_i$} (3,-.1);
  \end{tikzpicture}
  \caption{Saite dargestellt durch diskrete Massen.}
  \label{fig:2014-11-04}
\end{figure}
\begin{equation}
  \label{eq:224a}
  y_i(t) = \frac{1}{\Delta x_i} \int_{\Delta x_i} \diff x \, y(x,t)
  \stackrel{\Delta x \to 0}{\approx} y(x_i,t)
\end{equation}
Analog folgt für die Impulse
\begin{equation}
  \label{eq:224b}
  p_i(t) = \int_{\Delta x_i} \diff x \, \pi(x,t) \stackrel{\Delta x
    \to 0}{\approx} \pi(x_i,t) \Delta x_i.
\end{equation}
mit der Impulsdichte $\pi(x,t) = \frac{\partial \mathcal{L}}{\partial
  \dot{y}}$. Im Sinn der kanonischen Bewegungsgleichungen gehen wir zu
Operatoren über
\begin{align}
  \label{225a}
  \hat{y}_i &= \frac{1}{\Delta x_i} \int_{\Delta x_i} \diff x \, \hat{y}(x,t) \approx \hat{y}(x_i,t), \\
  \label{225b}
  \hat{p}_i &= \int_{\Delta x_i} \diff x \, \hat{\pi}(x,t) \approx \hat{\pi}(x_i,t) \Delta x_i.
\end{align}
Für die diskreten Variablen lässt sich die Quantisierung über die
Forderung der Vertauschungsregeln
\begin{equation}
  \label{eq:226}
  \boxed{
    [\hat{y}_i,\hat{p}_j] = \ii \hbar \delta_{ij} \mcomma [\hat{y}_i,
    \hat{y}_j] = [\hat{p}_i, \hat{p}_j] = 0
    }
\end{equation}
einführen.

Für das Beispiel der schwingenden Saite erhalten wir im Grenzwert
$\Delta x \to 0$
\begin{equation*}
  0 = [\hat{p}_i, \hat{p}_j] \eqrel{225b}{\to} [\hat{\pi}(x_i,t),
  \hat{\pi}(x_j,t)] \Delta x_i \cdot \Delta x_j = 0 \mcomma
\end{equation*}
also
\begin{equation}
  [\hat{\pi}(x_i,t), \hat{\pi}(x_j,t)] = 0 
  \label{eq:227a}  
\end{equation}
und durch analoges Vorgehen 
\begin{equation}
  [\hat{y}(x_i,t), \hat{y}(x_j,t)] = 0.
  \label{eq:227b} 
\end{equation}
Die  Bedingung in~\eqref{eq:226} fordert
\begin{align*}
  [ \hat{y}_i, \hat{p}_j ] = \ii \hbar \delta_{ij} \eqrel{225a}[225b]{\to} [\hat{y}(x_i,t),
  \hat{\pi}(x_j,t) ] \Delta x_j = \delta_{ij} \ii \hbar
\end{align*}
und ist für den Grenzfall $\Delta x_j \to 0$ genau dann erfüllt, wenn gilt
\begin{equation}
  [\hat{y}(x_i,t),\hat{\pi}(x_j,t)] = \ii \hbar \lim_{\Delta x_j \to 0}
  \frac{\delta_{ij}}{\Delta x_j} = \ii \hbar \delta(x_i - x_j) \mcomma
  \label{eq:227}
\end{equation}
denn 
\begin{align}
  \int_{\Delta x} \diff  x \, f(x) \frac{\delta_{ij}}{\Delta x_j} &= 
  \begin{cases}
    \frac{1}{\Delta x} \int \diff x \, f(x) \stackrel{\Delta x \to 0}{\to} f(x) \qquad &i = j \\
    0  \qquad &i \neq j
  \end{cases}  \notag \\
  \label{eq:228}
\end{align}

\subsection{Verallgemeinerung}

In Verallgemeinerung zum Beispiel der schwingenden Saite führt man die
Quantisierung ein, indem man für Felder $\phi_r$ und ihre Impulsdichte
$\pi_s$ postuliert
\begin{align}
  [\phi_r(\bm{r},t), \pi_s(\bm{r}',t)] &= \ii \hbar \delta(\bm{r} - \bm{r}') \delta_{rs},
  \label{eq:229a} \\
  [\phi_r(\bm{r},t), \phi_s(\bm{r}',t)] &= [\pi_r(\bm{r},t), \pi_s(\bm{r}',t)] = 0.
  \label{eq:229b}
\end{align}
Dies ist die \acct{kanonische Quantisierung} und enthält die
bosonischen Vertauschungsregeln, die -- wie der Name schon sagt -- auf
Bosonen führen werden. Später werden wir sehen, dass noch andere
Regeln benötigt werden, um eine sinnvolle physikalische Aussage zu
erhalten.

\section{Symmetrien und Erhaltungssätze}

Wir nehmen an, dass es eine kontinuierliche Symmetrie gibt, welche die
Wirkung invariant lässt
\begin{align}
  x^\mu &\to x'^\mu, \label{eq:230a} \\
  \phi_r(x^\mu) &\to \phi'_r(x'^\mu) \label{eq:230b}, \\
  \int \diff^4 x \, \mathcal{L}(\phi'_r,\partial'_\mu \phi'_r,x')
  &= \int \diff^4 x \, \mathcal{L}(\phi_r,\partial_\mu \phi_r, x).
  \label{eq:230c}
\end{align}
Dann lässt sich die Relation
\begin{equation}
  \label{eq:231a}
  \frac{\partial}{\partial x^\mu} 
  \left[
    \frac{\mathcal{L}}{\partial \phi_{r,\mu}} \diff \phi_r -
    \left(\frac{\mathcal{L}}{\partial \phi_{r,\mu}} \phi_{r,\nu} -
      \delta^{\mu}{}_\nu \mathcal{L} \right) \diff x^\nu
  \right]
  = 0
\end{equation}
ableiten, wobei gilt
\begin{align}
  \diff \phi_r &= \phi'_r(x') - \phi_r(x) \mcomma \label{eq:231b} \\ 
  \diff x^\mu &= x'^\mu - x^\mu \mpunkt \label{eq:231c}
\end{align}
Dies ist das \acct{Noether-Theorem.}

%%% Local Variables: 
%%% mode: latex
%%% mode: flyspell
%%% ispell-local-dictionary: "de_DE"
%%% TeX-master: "../QFT.tex"
%%% End: