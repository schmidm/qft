% Marcel Klett

Als Beispiel betrachten wir einen Wechselwirkungsterm der einen Feldoperator eines reellen Feldes $\phi$ enthalten soll,
\begin{equation*}
  \phi = \int \diff^3 \bm{k} \, a_{\bm{k}} f_{\bm{k}}(x) + a^\dagger_{\bm{k}} f^*_{\bm{k}}(x) \mpunkt
\end{equation*}
In zweiter Ordnung Störungstheorie erhalten wir
\begin{equation*}
  \int_{-\infty}^{\infty} \diff t_1  \int_{-\infty}^{\infty} \diff t_2 \, T(\phi(t_1) \phi(t_2)) \mpunkt
\end{equation*}
Durch die Zeitordnung treten darin Terme der Form
\begin{equation*}
  a_{\bm{k}_1}   a_{\bm{k}_2}, \quad   a_{\bm{k}_2}   a_{\bm{k}_1}, \quad a^\dagger_{\bm{k}_1}   a_{\bm{k}_2}, \quad  \ldots 
\end{equation*}
auf. Insbesondere kommt dadurch jede erdenkliche Kombination der einzelnen Erzeuger und Vernichter vor. Betrachten wir nun ein Matrixelement der Streumatrix
\begin{equation*}
  \Braket{\bm{k}_a|S|\bm{k}_b} \mcomma
\end{equation*}
so ist hierfür die Darstellung 
\begin{equation}
  \label{eq:stern}
  S_{ab} = \Braket{\bm{k}_a|a^\dagger_{\bm{k}_a} a_{\bm{k}_b}|\bm{k}_b}
\end{equation}
besonders einfach, denn
\begin{equation*}
  S_{ab} = \Braket{0|a_{\bm{k}_a} a^\dagger_{\bm{k}_a} a_{\bm{k}_b} a^\dagger_{\bm{k}_b}|0} \mpunkt
\end{equation*}
Damit ist jeweils nur eine Vertauschung 
\begin{align*}
  a_{\bm{k}_a} a^\dagger_{\bm{k}_a} &= \pm a^\dagger_{\bm{k}_a} a_{\bm{k}_a} + x \\
  a_{\bm{k}_b} a^\dagger_{\bm{k}_b} &= \pm a^\dagger_{\bm{k}_b} a_{\bm{k}_b} + y \\
\end{align*}
nötig, um die nichtverschwindenden Terme $x$ und $y$ zu erhalten, alle anderen führen zur Anwendung von Vernichtern auf den Vakuumszustand $\Ket{0}$ und damit verschwinden diese. Das Vorzeichen ist hierbei $+$ für Bosonen und $-$ für Fermionen (Kommutator/Antikommutator). Der Term in \eqref{eq:stern} ist gerade normalgeordnet. Eine Verallgemeinerung überlegt man sich leicht für höhere Potenzen der Operatoren. Unter einer Normalordnung höherer Potenzen versteht man die Form,
\[ a^\dagger_{\bm{k}_1} a^\dagger_{\bm{k}_2} \ldots  a_{\bm{k}_2}  a_{\bm{k}_1} \mcomma \]
 in der alle Erzeuger links und alle Vernichter rechts stehen. Die Reihenfolge der Indizes $1,2 \ldots$ ist nur für Fermionen relevant, bei bosonischen Operatoren kommutieren die einzelnen $a_{\bm{k}_i}$ repsketive $a^\dagger_{\bm{k}_i}$ untereinander.

\subsection{Wicksches Theorem}

Wir führen die \acct{Kontraktion} zweier Operatoren ein.
\begin{equation}
  \label{eq:432}
  \underbracket{AB} = T(AB) - \nOrd{AB}
\end{equation}
Von der Zeitordnung wissen wir, dass 
\begin{subequations}
  \begin{equation}
    \label{eq:433a}
    T(AB) = AB
  \end{equation}
  gilt, falls die Operatoren bereits zeitgeordnet sind, ansonsten gilt:
  \begin{equation}
    \label{eq:433b}
    T(AB) =
    \begin{cases}
      BA = AB + BA -AB = AB + [B,A] \quad &\text{Bosonen} \\
      -BA = AB - BA -AB = AB - \{B,A\} \quad &\text{Fermionen} \\
    \end{cases}
  \end{equation}
\end{subequations}
Analog gilt für die Normalordnung, sofern $AB$ bereits normalgeordnet ist
\[ \nOrd{AB} = AB \mpunkt \] Ist jedoch $B$ ein Erzeuger und $A$ ein Vernichter, so gilt:
\begin{equation}
  \label{eq:434}
  \nOrd{AB} =
  \begin{cases}
    AB + [B,A] \quad &\text{Bosonen} \\
    AB - \{B,A \} \quad &\text{Fermionen} \\
  \end{cases}
\end{equation}
Für die Kontraktion verbleibt:
\begin{equation*}
  \underbracket{AB} = \begin{cases}
    a [B,A] \quad &\text{Bosonen} \\
    -a \{B,A \} \quad &\text{Fermionen} \\
  \end{cases}
  \quad a \in \left\{1,0,-1 \right\}
\end{equation*}
Die Kommutatoren/Antikommutatoren der Feldoperatoren sind nach den Quantisierungsregeln \emph{aller} behandelten Felder \emph{keine} Operatoren sondern komplexe Zahlen, welche gegebenenfalls noch mit Einheiten multipliziert sind, also 
\begin{equation}
  \label{eq:435}
  \braket{0|\underbracket{AB}|0} = \underbracket{AB} \mcomma
\end{equation}
weiterhin gilt
\begin{equation}
  \label{eq:436}
  \Braket{0|\nOrd{AB}|0} = 0
\end{equation}
und damit
\begin{equation}
  \label{eq:437}
  \Braket{0|T(AB)|0} \eqrel{eq:432}[eq:436]{=} \underbracket{AB} \mpunkt
\end{equation}
Für ein zeitgeordnetes Produkt erhält man damit
\begin{equation}
  \label{eq:438}
  T(AB) \eqrel{eq:432}{=} \nOrd{AB} + \underbracket{AB} \eqrel{eq:437}{=}
  \nOrd{AB} + \Braket{0|T(AB)|0} \mcomma
\end{equation}
also das normalgeordnete Produkt addiert mit einer komplexen Zahl, nämlich nach~\eqref{eq:437} den Vakuumserwartungswert von $T(AB)$. Die Verallgemeinerung lautet (ohne Beweis)
\begin{align}
  \begin{aligned}
    T(A_1 A_2 \ldots A_n) &= \nOrd{A_1 A_2 \ldots A_n} \\
    & + \nOrd{\underbracket{A_1A_2} \ldots A_n} + \nOrd{A_1 \underbracket{A_2 A_3} \ldots A_n} + \ldots \\
    &+ \nOrd{\underbracket{A_1A_2} \underbracket{A_3A_4} \ldots A_n} + \nOrd{\underbracket{A_1 A_2} A_3 \underbracket{A_4 A_5}\ldots A_n} + \ldots \\
    &+ \nOrd{\underbracket{A_1A_2} \underbracket{A_3A_4} \underbracket{A_5A_6} \ldots A_n} + \nOrd{\underbracket{A_1 A_2} \underbracket{A_3 A_4} A_5 \underbracket{A_6 A_7}\ldots A_n} + \ldots \mcomma
  \end{aligned}
  \label{eq:439}
\end{align}
wobei in der zweiten Zeile in \eqref{eq:440} jeweils alle Möglichkeiten einer Kontraktion in der Normalordnung auftreten, in der zweiten Zeile alle Möglichkeiten zweier Kontraktionen, in der dritten Zeile alle Möglichkeiten dreier Kontraktionen usw.\, Dies ist das \acct{Wicksche Theorem}. Treten bereits normalgeordnete Terme in \eqref{eq:439} auf, zum Beispiel $T(\nOrd{AB} CD \nOrd{EF})$, so entfallen in \eqref{eq:439} die Kontraktionen, welche zwei Operatoren aus demselben normalgeordneten Produkt enthalten würden.

\section{Propagatoren}

Propagatoren stellen die trivialste (nämlich keine) Wechselwirkung dar. Als Beispiel betrachten wir ein Teilchen, das sich zum Zeitpunkt $t_1$ am Ort $\bm{r}_1$ befinden soll. Es wird dabei durch den Zustand
\begin{equation}
  \label{eq:440}
  \Ket{\psi(\bm{r}_1,t_1)} = \phi^\dagger(\bm{r}_1,t_1) \Ket{0}
\end{equation}
beschreiben. Wir wollen nun die Wahrscheinlichkeit wissen, mit der das Teilchen zum Zeitpunkt $t_2$ am Ort $\bm{r}_2$ ist. Dazu benötigt man
\begin{equation}
  \label{eq:441}
  \Braket{\psi(\bm{r}_2,t_2)|\psi(\bm{r}_1,t_1)} \eqrel{eq:440}{=} \Braket{0|\phi(\bm{r}_2,t_2) \phi^\dagger(\bm{r}_1,t_1)|0} \mpunkt
\end{equation}
Der Feynman-Propagator
\begin{equation}
  \label{eq:442}
  \Delta_F(x-x') = - \ii \Braket{0|T(\phi(x) \phi^\dagger(x'))|0} \eqrel{eq:437}{=} - \ii \underbracket{\phi(x) \phi^\dagger(x')}
\end{equation}
tritt typischerweise in Zeitentwicklungen auf. Seine Bedeutung wird klar, wenn wir die beiden Fälle $t > t'$ und $t < t'$  und die dazugehörigen Orts-Zeit-Diagramme in Abbildung \ref{fig:2014-12-16-1} betrachten.
\begin{figure}[tb]
  \centering
  \begin{tikzpicture}[gfx]
    \begin{scope}
      \node[text width=4cm,align=center,above] at (1.5,3) {%
        Transport eines Teilchens von $x'$ nach $x$};
      \draw[<->] (0,2.5) node[left] {Zeit} |- (3,0) node[right] {Ort};
      \node[DarkOrange3,dot] (A) at (2.5,2) {};
      \node[DarkOrange3,dot] (B) at (1,1) {};
      \draw[DarkOrange3,dotted] (A |- 0,0) node[below] {$x$} -- (A)
      -- (A -| 0,0) node[left] {$t$};
      \draw[DarkOrange3,dotted] (B |- 0,0) node[below] {$x'$} -- (B)
    -- (B -| 0,0) node[left] {$t'$};
    \draw[DarkOrange3,arrow inside] (B) -- node[below right] {$+q$} (A);
  \end{scope}
  \begin{scope}[shift={(5,0)}]
    \node[text width=4cm,align=center,above] at (1.5,3) {%
      Transport eines Antiteilchens von $x$ nach $x'$};
    \draw[<->] (0,2.5) node[left] {Zeit} |- (3,0) node[right] {Ort};
    \node[MidnightBlue,dot] (A) at (2.5,2) {};
    \node[MidnightBlue,dot] (B) at (1,1) {};
    \draw[MidnightBlue,dotted] (A |- 0,0) node[below] {$x'$} -- (A)
    -- (A -| 0,0) node[left] {$t'$};
    \draw[MidnightBlue,dotted] (B |- 0,0) node[below] {$x$} -- (B)
    -- (B -| 0,0) node[left] {$t$};
    \draw[MidnightBlue,arrow inside] (B) -- node[below right] {$-q$} (A);
  \end{scope}
\end{tikzpicture}
  \caption{Links: $t > t'$: $\Braket{0|\phi(x)\phi^\dagger(x')|0}$. Rechts: $t < t'$: $\Braket{0|\phi^\dagger(x')\phi(x)|0}$.}
  \label{fig:2014-12-16-1}
\end{figure}

\subsection{Feynman-Propagator für das Klein-Gordon-Feld}

Wir rufen uns die Feldoperatoren des Klein-Gordon-Feldes in Erinnerung,
\begin{equation*}
  \phi(\bm{r},t) \eqrel{eq:312a}{=} \frac{2m c^2}{\hbar} \int \diff^3 \bm{k} \, a_{\bm{k}} f_{\bm{k}}(\bm{r},t) + c^\dagger_{\bm{k}} f^*_{\bm{k}}(\bm{r},t)
\end{equation*}
und berechnen damit den Feynman-Propagator
\begin{align*}
  \Delta_F(x-x') &\eqrel{eq:442}{=} - \ii \Braket{0|T(\phi(x)\phi^\dagger(x'))|0} \\
  &=
  \begin{multlined}[t]
    - \ii \frac{2 m c^2}{\hbar} \int \diff^3 \bm{k} \int  \diff^3 \bm{k}' \Biggl\{ f_{\bm{k}}(\bm{r},t) f_{\bm{k}'}^*(\bm{r}',t') \underbrace{\Braket{0|a_{\bm{k}} a^\dagger_{\bm{k}'}|0}}_{=\delta(\bm{k} - \bm{k}')} \\
    + f_{\bm{k}}(\bm{r},t) f_{\bm{k}'}(\bm{r}',t') \underbrace{\Braket{0|a_{\bm{k}} c_{\bm{k}'}|0}}_{=0} + 
    f_{\bm{k}}^*(\bm{r},t) f_{\bm{k}'}^*(\bm{r}',t') \underbrace{\Braket{0|c_{\bm{k}}^\dagger a^\dagger_{\bm{k}'}|0}}_{=0} \\
    + f_{\bm{k}}^*(\bm{r},t) f_{\bm{k}'}(\bm{r}',t') \underbrace{\Braket{0|c_{\bm{k}}^\dagger c_{\bm{k}'}|0}}_{=0} \Biggr\} \Theta(t-t') \\
    - \ii \frac{2mc^2}{\hbar} \int \diff^3 \bm{k} \int  \diff^3 \bm{k}' f_{\bm{k}'}(\bm{r}',t') f^*_{\bm{k}}(\bm{r},t) \delta(\bm{k} - \bm{k}') \Theta(t'-t)
  \end{multlined} \\
  &\eqrel{eq:310b}{=} - \ii mc^2 \frac{1}{(2\pi)^3} \int \diff^3 \bm{k} \frac{1}{E(\bm{k})} \left\{ \ee^{-\ii k_\mu (x^\mu - x'^{\mu})} \Theta(t-t')
    + \ee^{\ii k_\mu (x^\mu - x'^{\mu})} \Theta(t'-t) \right\} \mpunkt
\end{align*}
Oder anders geschrieben,
\begin{equation}
  \label{eq:443}
  \Delta_F(x-x') = -\ii \int \frac{\diff^3 \bm{k}}{(2\pi)^3} \frac{mc^2}{E(\bm{k})} \left\{ \ee^{-\ii k_\mu (x^\mu - x'^{\mu})} \Theta(t-t')
    + \ee^{\ii k_\mu (x^\mu - x'^{\mu})} \Theta(t'-t) \right\} \mpunkt
\end{equation}
Die beiden Summanden lassen sich auch einheitlich schreiben. Dazu machen wir einen kleinen Ausflug in die Funktionalanalysis und betrachten das Integral
\begin{equation*}
  \int_{c_1} \frac{\ee^{-\ii k_0 (x^0 - x'^{0})} \ee^{-\ii k_i (x^i - x'^{i})}}{k_\mu k^\mu - \left( \frac{mc}{\hbar}\right)^2} \diff k_0
\end{equation*}
Indem wir den Nenner etwas umschreiben, können wir die Polstellen verdeutlichen
\begin{equation*}
  k_\mu k^\mu  - \left( \frac{mc}{\hbar}\right)^2 = \left(k_0 - \sqrt{ (k_i)^2 + (mc/\hbar)^2}\right)  \left(k_0 + \sqrt{ (k_i)^2 + (mc/\hbar)^2}\right) \mpunkt
\end{equation*}
Das Ergebnis des Integrals, das sich für den Weg aus Abbildung~\ref{fig:2014-12-16-2} ergibt, ist dann das Residuum für den Pol bei $E/c\hbar$.
\begin{figure}[tb]
  \centering
  \begin{tikzpicture}[gfx]
    \draw[->] (-4,0) -- (4,0) node[right] {$\Re k_0$};
    \draw[->] (0,-3.7) -- (0,2) node[above] {$\Im k_0$};
    \node[MidnightBlue,dot,label={[MidnightBlue]above:$k_0=-\sqrt{(\frac{mc}{\hbar})^2+\sum_i (k_i)^2} = -\frac{E}{c\hbar}$}] at (-3,0) {};
    \node[MidnightBlue,dot,label={[MidnightBlue]above:$k_0= \sqrt{(\frac{mc}{\hbar})^2+\sum_i (k_i)^2} =  \frac{E}{c\hbar}$}] at (3,0) {};
    \draw[DarkOrange3,arrow inside={end=<}] (-3.5,0) -- (-3.2,0) arc(-180:0:.2) -- (2.8,0) arc(180:0:.2) -- (3.5,0)
    arc(0:-180:3.5);
  \end{tikzpicture}
  \caption{Integrationsweg von $c_1$ der den Pol bei $E/c\hbar$ enthält.}
  \label{fig:2014-12-16-2}
\end{figure}
\begin{align}
  \notag
  \int_{c_1} \frac{\ee^{-\ii k_0 (x^0 - x'^{0})} \ee^{\ii k_i (x^i - x'^{i})}}{k_\mu k^\mu - \left( \frac{mc}{\hbar}\right)^2} \diff k_0 &= \frac{2 \pi \ii}{k_0 + \sqrt{(k^i)^2 + (mc/\hbar)^2}} \ee^{-\ii k_0 (x^0 - x'^{0})} \ee^{-\ii k_i (x^i - x'^{i})} \\
  = 2 \pi \ii \frac{\ee^{-\ii k_\mu (x^\mu - x'^\mu)}}{2k_0} &= \frac{2\pi \ii}{2} c \hbar \frac{\ee^{-\ii k_\mu (x^\mu - x'^\mu)}}{E(\bm{k})} \mcomma \label{eq:444}
\end{align}
oder anders ausgedrückt
\begin{equation}
  \label{eq:445}
  - \frac{2mc}{\hbar} \int_{c_1} \frac{\diff^4 k}{(2\pi)^4} \frac{\ee^{-\ii k_\mu (x^\mu - x'^\mu)}}{k_\mu k^\mu - (mc/\hbar)^2} \eqrel{eq:444}{=} - \ii \int \frac{\diff^3 \bm{k}}{(2\pi)^3} \frac{mc^2}{E(\bm{k})} \ee^{-\ii k_\mu (x^\mu - x'^\mu)} \mpunkt
\end{equation}
Nun kann man den Integrationsweg noch vereinfachen, indem man die Pole infenitesimal in der imaginären Ebene verschiebt (siehe Abbildung \ref{fig:2014-12-16-3}),
\begin{align*}
  &\left(k_0 - \sqrt{(k_i)^2 + (mc/\hbar)^2} + \frac{\ii \varepsilon}{2\sqrt{ (k_i)^2 + (mc/\hbar)^2}}\right) \times \\
  &\left(k_0 + \sqrt{(k_i)^2 + (mc/\hbar)^2} - \frac{\ii \varepsilon}{2\sqrt{(k_i)^2 + (mc/\hbar)^2}}\right) \\
  &= k_0^2 - k_i k^i + (mc/\hbar)^2 + \ii \varepsilon + \mathcal{O}(\varepsilon^2)
\end{align*}
\begin{figure}[tb]
  \centering
  \begin{tikzpicture}[gfx]
    \node[MidnightBlue,dot] (A) at (-3,0) {};
    \node[MidnightBlue,dot] (B) at (3,0) {};
    \draw[MidnightBlue,->] (A) -- ++(0,.5);
    \draw[MidnightBlue,->] (B) -- ++(0,-.5);
    \draw[DarkOrange3,arrow inside={end=<}] (-3.5,0) -- (-3.2,0) arc(-180:0:.2) -- (2.8,0) arc(180:0:.2) -- (3.5,0);
    \draw[DarkOrange3,dashed] (3.5,0) arc(0:-180:3.5);
    \node[pin={[text width=3cm]right:Integrand verschwindet auf diesem Abschnitt}] at (-30:3.5) {};
  \end{tikzpicture}
  \caption{Neuer Integrationsweg}
  \label{fig:2014-12-16-3}
\end{figure}
Damit ergibt sich für die Kontur $c_1$
\begin{equation}
  \label{eq:446}
  \int_{c_1} \to \lim_{\varepsilon \to 0} \int_{\infty}^{-\infty} \frac{\ee^{-\ii k_\mu (x^\mu - x'^\mu)} \diff k_0}{k_\mu k^\mu - (mc/\hbar)^2 + \ii \varepsilon} = - \lim_{\varepsilon \to 0} \int_{-\infty}^{\infty} \frac{\ee^{-\ii k_\mu (x^\mu - x'^\mu)} \diff k_0}{k_\mu k^\mu - (mc/\hbar)^2 + \ii \varepsilon}
\end{equation}
und es folgt
\begin{equation}
  \label{eq:447}
  - \ii \int \frac{\diff^3 \bm{k}}{(2\pi)^3} \frac{mc^2}{E(\bm{k})} \ee^{-\ii k_\mu (x^\mu - x'^\mu)} \eqrel{eq:445}[eq:446]{=} 2 \frac{mc}{\hbar} \int \frac{\diff^4 k}{(2 \pi)^4} \frac{\ee^{-\ii k_\mu (x^\mu - x'^\mu)}}{k_\mu k^\mu - (mc/\hbar)^2 + \ii \varepsilon} \mpunkt
\end{equation}
Für den zweiten Summanden mit dem Term $\Theta(t'-t)$ findet man \emph{exakt dieselbe} Darstellung, man kann also ohne Einschränkung der $\Theta$-Funktionen schreiben 
\begin{equation}
  \label{eq:448}
  \Delta_F(x-x') \eqrel{eq:447}[eq:443]{=} 2 \frac{mc}{\hbar} \int \frac{\diff^4 k}{(2 \pi)^4}  \frac{\ee^{-\ii k_\mu (x^\mu - x'^\mu)}}{k_\mu k^\mu - (mc/\hbar)^2 + \ii \varepsilon}
\end{equation}
Der $\lim_{\varepsilon \to 0}$ wird oft nicht geschrieben, ist aber immer im Hinterkopf zu behalten!

\subsection{Dirac- und Maxwellfeld}

Vollkommen analog kann man Propagatoren für das Diracfeld aufstellen
\begin{align}
  \notag
  S_F(x-x') &= -\ii \underbracket{\underline\psi(x) \overline\psi(x')} \\
  &= -\ii \Braket{0|\underline\psi(x) \overline\psi(x')| 0} \notag \\
  &= \int \frac{\diff^4 k}{(2\pi)^4} \frac{\slashed{k} + \frac{mc}{\hbar}}{k_\mu k^\mu - \left( \frac{mc}{\hbar} \right)^2 + \ii \varepsilon} \mpunkt
  \label{eq:449}
\end{align}
Für das Maxwellfeld, das heißt für den Photonenpropagator, ergibt sich
\begin{equation}
  \label{eq:450}
  F_{F_{\alpha\beta}} (x-x') = - \int \frac{\diff^4 k}{(2\pi)^4} \frac{\hbar \mu_0 c}{k_\mu k^\mu + \ii \varepsilon} \ee^{\ii k_\mu (x^\mu - x'^\mu)} \eta_{\alpha \beta} + \textrm{Eichterm}   \mpunkt
\end{equation}