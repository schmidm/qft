% Michael Schmid

\chapter{Kurze Einführung in die relativistische Quantenmechanik}

\section{Grundlagen der SRT}

\subsection{Experimentelle Tatsachen und Konsequenzen}

Experimente zeigen, dass die Lichtgeschwindigkeit endlich ist und in
jedem Inertialsystem zum gleichen Wert gemessen wird. Dies
widerspricht jedoch der Galileitransformation, denn nach dieser
berechnet sich im Falle zweier zueinander bewegter Koordinatensysteme
(siehe Abbildung~\ref{fig:2014-10-14-1})
\begin{subequations}
  \begin{align}
    \label{eq:2014-10-14-1}
    \bm{r}' &= \bm{r} - \bm{a} - \bm{u} t \mcomma \\
    \label{eq:2014-10-14-2}
    \dot{\bm{r}}' &= \dot{\bm{r}} - \bm{u} \mpunkt
  \end{align}
\end{subequations}

\begin{figure}[tb]
  \centering
  \begin{tikzpicture}[gfx]
    % K
    \node at (1,3) {$K$};
    \draw[->] (0,0) -- (0,2.5) node [left] {$y$}; 
    \draw[->] (0,0) -- (2.5,0) node [right] {$x$}; 
    % K'
    \node at (4,3.5) {$K'$};
    \draw[->] (3,0.5) -- (3,3) node [left] {$y'$}; 
    \draw[->] (3,0.5) -- (5.5,0.5) node [right] {$x'$}; 
    % Vektorzeug
    \draw[->,DarkOrange3] (0,0) -- node[above] {$\bm{a} + \bm{u}t$} (3,0.5);
    \draw[->,Purple] (3,2.5) -- (4,2.5) node[right] {$\bm{u}$};
    \draw[->,MidnightBlue] (0,0) -- node[above] {$\bm{r}$} (2,2);
    \draw[<-,MidnightBlue] (2,2) -- node[above right] {$\bm{r}'$} (3,0.5);
  \end{tikzpicture}
  \caption{Galileitransformation von Inertialsystem $K$ nach
    $K'$. Das Inertialsystem $K'$ bewege sich mit $\bm{u} = u_0
    \bm{e}_x$ relativ zu $K$.}
  \label{fig:2014-10-14-1}
\end{figure}

Auch die elektromagnetischen Felder transformieren sich nicht nach der
Galileitransformation, sondern nach der Lorentztransformation,
andernfalls müssten in jedem Inertialsystem die Maxwellgleichungen
anders lauten.

\begin{notice}[Einstein, 1905:]
  Anwendung der Lorentztransformation auf die Mechanik gibt die
  richtige Physik wieder.
\end{notice}

Für das genannte Beispiel lautet die Lorentztransformation
\begin{subequations}
  \begin{align}
    t' &= \gamma \left( t- \frac{\beta}{c} x \right) \mcomma \\
    x' &= - \gamma \beta c t + \gamma x \mcomma
    \intertext{wobei}
    \beta &= \frac{u}{c} \mcomma \\
    \gamma &= \frac{1}{\sqrt{1-\beta^2}} \mpunkt
  \end{align} 
\end{subequations}
Es zeigt sich, dass sowohl die Zeit als auch der Ort transformiert werden
müssen.

\subsection{Geeignete Notation}

Da die Zeit kein geeigneter Bahnparameter ist, fassen wir diese mit
dem Ort zu einem \acct{Vierervektor} zusammen
\begin{align}
  x^\mu
  &= \begin{pmatrix} ct \\ x \\ y \\ z \end{pmatrix}
  = \begin{pmatrix} x^0 \\ x^1 \\ x^2 \\ x^3 \end{pmatrix}
  = \begin{pmatrix} ct \\ \bm{r}  \end{pmatrix}
\end{align}
Dabei gilt meist:
\begin{itemize}
  \item griechische Indizes zählen von  $0 \ldots 3 \mcomma$
  \item lateinische Indizes zählen von  $1 \ldots 3 \mpunkt$
\end{itemize}
Ein \acct{kontravarianter Vektor} wird durch $x^\mu$ beschreiben. Das
zugehörige Element im Dualraum, der \acct{kovariante Vektor} wird mit
$x_\mu$ bezeichnet.

Die Metrik der SRT  ist
\begin{align}
  \eta^{\mu\nu} &=  \eta_{\mu\nu}
  = \begin{pmatrix}
  1 & 0 & 0 & 0\\
  0 & -1& 0 & 0\\
  0 & 0 & -1& 0\\
  0 & 0 & 0 &-1\\
  \end{pmatrix} \mpunkt
\end{align}
Mit ihr wechselt man von kontra- zu kovarianten Komponenten und vice
versa,
\begin{align}
  x_{\mu} &= \eta_{\mu \nu} x^\nu \quad \text{und} \quad x^\mu =  \eta^{\mu \nu} x_\nu . 
\end{align}
Für das Skalarprodukt gilt in dieser Schreibweise
\begin{align}
  \nonumber
  x_\mu y^\mu &= x_\mu \eta^{\mu\nu} y_\nu \\
  \nonumber
  &= x^\mu \eta_{\mu\nu} y^\nu \\
  &= x^0 y^0 - x^1 y^1 - x^2 y^2 - x^3 y^3.
\end{align}

\subsection{Lorentztransformation}

Das vorherige Beispiel aus den Gleichungen~\eqref{eq:2014-10-14-1} bis
\eqref{eq:2014-10-14-2} lautet in der Vierer-Schreibweise
\begin{align}
  \begin{pmatrix}
    ct' \\ x' \\ y' \\ z'
  \end{pmatrix} &=
  \underbrace{\begin{pmatrix}
      \gamma & - \beta \gamma & 0 & 0 \\
      - \beta \gamma & \gamma & 0 & 0 \\
      0 & 0 & 1 & 0 \\
      0 & 0 & 0 & 1
    \end{pmatrix}}_{\Lambda_x}
  \begin{pmatrix}
    ct \\ x \\ y \\ z
  \end{pmatrix}.
\end{align}
Die Lorentztransformationen bilden ein Gruppe. In der
Matrixschreibweise erfüllen sie
\begin{subequations}
  \begin{align}
    \Lambda^T \eta \Lambda &= \eta \mcomma \\
    \label{eq:2014-10-14-2.0}
    \Lambda^\mu{}_{\varrho} \eta_{\mu \nu} \Lambda^\nu{}_{\sigma} &= \eta_{\varrho \sigma}
    \intertext{mit}
    \nonumber
    \Lambda^\mu{}_{\varrho} &= \left( \Lambda_{\varrho}^{\phantom{a}\mu}  \right)^T \mpunkt
  \end{align}
\end{subequations}
Ein Vektor oder Lorentzvektor transformiert sich nach einer
Lorentztransformation
\begin{align}
  \label{eq:2014-10-14-3}
  {x'}^\mu &= \Lambda^\mu{}_{\nu} x^\nu \mpunkt
\end{align}
Bei Tensoren höherer Stufe transformiert sich jeder Index nach der
Form~\eqref{eq:2014-10-14-3}
\begin{align}
  A'^{\alpha \beta \gamma}{}_{\delta \varepsilon}
  &= \Lambda^{\alpha}{}_{\mu}\Lambda^{\beta}{}_{\nu}\Lambda^{\gamma}{}_{\xi}\Lambda_{\delta}^{\phantom{a}\varrho}
  \Lambda_{\varepsilon}^{\phantom{a}\sigma}{A}^{\mu \nu \xi}{}_{\varrho \sigma} \mpunkt
\end{align}
Eine invariante skalare Größe unter Lorentztransformation wird als
\acct{Lorentzskalar} bezeichnet, der zum Beispiel aus der Kontraktion zweier Vektoren gebildet werden kann,
\begin{align}
  \nonumber
  {x'}^\mu {x'}_{\mu} &=  {x'}^{\mu } \eta_{\mu \nu} {x'}^{\nu} \\
  \nonumber
  &\eqrel{eq:2014-10-14-3}{=} \Lambda^{\mu}{}_{\varrho} \eta_{\mu \nu} \Lambda^{\nu}{}_{\sigma} x^\varrho x^\sigma \\
  \nonumber
  &\eqrel{eq:2014-10-14-2.0}{=} \eta_{\varrho \sigma} x^\varrho x^\sigma \\
  &= x^\varrho x_{\sigma} \mpunkt 
\end{align}

\subsection{Relativistische Kinematik} 

Ein wichtiger Lorentzskalar ist das Längenelement mit dem man die
Abstände zwischen zwei Punkten der Raumzeit misst,
\begin{align}
  \nonumber
  \diff s^2 &= c^2 \diff \tau^2  = \diff x^\mu \diff x_\mu \\
  \nonumber
  &= c^2 \diff t^2 - \diff \bm{r}^2 \\
  \label{eq:2014-10-14-5}
  &= (\diff x^0)^2 - (\diff x^1)^2 - (\diff x^2)^2 - (\diff x^3)^2 \mpunkt
\end{align}
Daraus folgt, dass $\tau$ invariant unter Lorentztransformation ist
(Lorentzskalar).

In einem Koordinatensystem, in dem man sich selbst nicht bewegt ($\diff
x^i = 0$) gilt
\begin{align}
  \nonumber
  c^2 \diff \tau^2 &= c^2 \diff t^2,
\end{align}
Die Variable $\tau$ hat hier die Bedeutung der Zeit. Sie wird daher als
\acct{Eigenzeit} bezeichnet.

In diesen Zusammenhang stellt sich die Frage nach einer sinnvollen
Definition einer Geschwindigkeit. Diese sollte die folgenden
Eigenschaften erfüllen:
\begin{itemize}
\item Sie sollte ein Vierervektor sein.
\item Ableitungen von $x^\mu$ nach $t$ kommen nicht in Frage, diese sind keine Lorentzskalare.
\item Eine Ableitung nach $\tau$ ist jedoch möglich:
  \begin{subequations}
    \begin{align}
      \nonumber
      u^\mu &= \frac{\diff}{\diff \tau} x^\mu = \frac{\diff}{\diff \tau}
      \begin{pmatrix}
        ct \\ \bm{r}
      \end{pmatrix} \\
      &=
      \begin{pmatrix}
        c \frac{\diff t}{\diff \tau} \\ \frac{\diff \bm{r}}{\diff t}\frac{\diff t}{\diff \tau}
      \end{pmatrix} = \frac{\diff t}{\diff \tau}
      \begin{pmatrix}
        c \\ \dot{\bm{r}} 
      \end{pmatrix} \mpunkt
      \intertext{Mit}
      \nonumber
      c^2 \left( \frac{\diff \tau}{\diff t}  \right)^2 &\eqrel{eq:2014-10-14-5}{=} c^2 - \dot{\bm{r}}^2  = c^2 - \bm{v}^2 \mcomma \\
      \nonumber
      \left( \frac{\diff \tau}{\diff t}  \right)^2 &= 1 - \left( \frac{v}{c} \right)^2 \mcomma \\
      \implies \frac{\diff t}{\diff \tau} &= \gamma \mcomma \\
      \implies u^\mu &= \gamma
      \begin{pmatrix}
        c \\ \bm{v}
      \end{pmatrix} \mpunkt
    \end{align}
  \end{subequations}
\end{itemize}

Damit sind wir in der Lage, einen \acct{Viererimpuls} zu identifizieren,
\begin{align}
  p^\mu &= m u^\mu = m \gamma
  \begin{pmatrix}
    c \\ \bm{v}
  \end{pmatrix} =
  \begin{pmatrix}
    m \gamma c \\ \bm{p}
  \end{pmatrix} \mpunkt
\end{align}
Hierbei haben wir $\bm{p} = m \gamma \bm{v}$ für den
räumlichen Anteil verwendet. Betrachten wir nur die nullte Komponente, d.h.
\begin{align}
  \nonumber
  p^0 &= m \gamma c \\
  \nonumber
  p^0 c &= m \gamma c^2 = \frac{mc^2}{\sqrt{1- \frac{v^2}{c^2}}} \approx mc^2 + \frac{m v^2}{2} + \ldots
\end{align}
Die Entwicklung in eine Taylorreihe zeigt, dass wir die Ruheenergie,
die kinetische Energie eines Teilchens und relativistische Korrekturen
erhalten. Somit ergibt sich eine sinnvolle Definition der Energie. 
Man interpretiert entsprechend
\begin{align}
  p^0 &= \frac{E}{c}
\end{align}
mit der Energie $E$ einer Punktmasse. Daraus folgt
\begin{align}
  \nonumber
  p^\mu p_\mu &= \frac{E^2}{c^2} - \bm{p}^2 = m^2 \gamma^2 \left( c^2 - v^2  \right) = m^2 c^2 \mpunkt
\end{align}
Somit sind wir in der Lage, die \acct{relativistische Energie-Impuls
  Beziehung} anzugeben:
\begin{subequations}
  \begin{align}
    \label{eq:2014-10-14-6a}
    E^2 &= m^2 c^4 + c^2 \bm{p}^2 \mcomma \\
    \label{eq:2014-10-14-6b}
    E &= \sqrt{ m^2 c^4 + c^2 \bm{p}^2  } \mpunkt
  \end{align}
\end{subequations}

\section{Die Klein-Gordon-Gleichung}

Die Schrödingergleichung kann aus Sicht der Relativitätstheorie nicht
korrekt sein, denn sie ist nicht invariant unter einer
Lorentztransformation. Diese wurde gerade aus der klassischen
Energie-Impuls-Beziehung
\begin{align}
  E &= \frac{\bm{p}^2}{2 m} + V(\bm{r}) = H(\bm{r},\bm{p})
\end{align}
unter Verwendung der Jordanschen Ersetzungsregeln
\begin{align}
  E & \to \ii \hbar \partial_t \mcomma \nonumber \\
  \bm{p} & \to \frac{\hbar}{i} \nabla  \label{eq:118}
\end{align}
motiviert. 

Aufstellen einer Lorentz-invarianten quantenmechanischen Gleichung:
%\begingroup
%\advance\leftskip by 1cm\relax

Verwende die Ersetzungsregeln~\eqref{eq:118} in der
relativistischen Energie-Impuls Beziehung~\eqref{eq:2014-10-14-6a},
aber nicht~\eqref{eq:2014-10-14-6b}, da sonst die Wurzel aus einem
Differentialoperator benötigt wird (unendlich hohe Ableitungen):
\begin{align}
  - \hbar^2 \partial_t^2 \psi &= \left[ m^2 c^4 - \hbar c^2 \nabla^2 \right] \psi
\end{align}
Wir erhalten die \acct{Klein-Gordon-Gleichung}.\par
%\endgroup
Diese Schreibweise lässt sich noch vereinfachen
\begin{align}
  \frac{1}{c^2} \partial_t^2 - \nabla^2 &= \frac{\partial^2}{\partial(ct)^2} - \partial_x^2 -\partial_y^2 -\partial_z^2 \nonumber \\
  &= \partial^\mu \eta_{\mu \nu} \partial^\nu \nonumber \\
  &= \partial_\mu \partial^\mu 
\end{align}
oder mit dem D'Alembert-Operator
\begin{align}
  \quabla &\equiv \partial_\mu \partial^\mu 
\end{align}
erhalten wir
\begin{subequations}
  \begin{align}
    \implies \left[ \partial_\mu \partial^\mu + \left( \frac{mc}{\hbar}  \right)^2 \right] \psi &= 0 \\
    \implies \left[ \quabla + \left( \frac{mc}{\hbar}  \right)^2 \right] \psi &= 0.\label{eq:2014-10-14-8}
  \end{align}
\end{subequations}
Dies ist die eigentliche Darstellung der Klein-Gordon-Gleichung. Sie
besitzt folgende unerwünschte Eigenschaften:
\begin{itemize}
\item Lösung ist das Skalarfeld $\psi$. Die Lösungen beschreiben ein
  Teilchen ohne Spin, d.h.\ wir erhalten keine geeignete Gleichung zur
  Beschreibung eines Elektrons.
\item Als Lorentzskalar ist dieses $\psi$ invariant unter
  Lorentztransformationen.
\item Anders als in der Quantenmechanik gefordert, benötigt man zwei
  Anfangsbedingungen, nämlich
  \begin{equation*}
    \psi(\bm{r},t=0) = \ldots \mcomma \qquad  \dot{\psi}(\bm{r},t=0) = \ldots \mcomma
  \end{equation*}
  um die Zeitentwicklung berechnen zu können, d.h.\ es tritt ein Widerspruch zu den Postulaten der Quantenmechanik auf!
\item Die Lösungen $\psi$ können auf negative Wahrscheinlichkeitsdichten
  führen. Dies ist jedoch problematisch für die Interpretation der
  Wellenfunktion!
\end{itemize}

\minisec{Energiespektren (freies Teilchen)}

Die Klein-Gordon-Gleichung~\eqref{eq:2014-10-14-8} besitzt als freie
Lösung die ebenen Wellen
\begin{subequations}
  \begin{align}
    \psi(\bm{r},t) &= \ee^{\frac{\ii}{\hbar}(Et- \bm{p} \bm{r})}
    \intertext{mit}
    E &= \pm \sqrt{p^2 c^2 + m^2 c^4},
  \end{align}
\end{subequations}
d.h.\ zu jedem Impuls $\bm{p}$ oder Wellenvektor $\bm{k} =
\bm{p}/\hbar$ gibt es,
\begin{itemize}
\item eine Lösung positiver Energie,
\item eine Lösung negativer Energie.
\end{itemize}
In Abbildung~\ref{fig:2014-10-14-2} ist dies veranschaulicht. Für die
Energie sind Werte von $E \to - \infty$ möglich, d.h.\ es liegt ein
Stabilitätsproblem vor!

\begin{figure}[tb]
  \begin{tikzpicture}[gfx]
    \draw[->] (-3,0) -- (3,0);
    \draw[->] (0,-2) -- (0,2) node[left] {$E$};
    \draw[DarkOrange3] (-3,1) -- (3,1) node[right] {$+mc^2$};
    \draw[DarkOrange3] (-3,-1) -- (3,-1) node[right] {$-mc^2$};
    \fill[pattern=north east lines,pattern color=DarkOrange3] (-3,1) rectangle (3,1.8);
    \fill[pattern=north east lines,pattern color=DarkOrange3] (-3,-1) rectangle (3,-1.8);
    \node[pin={above:kontinuierliches Spektrum}] at (-2,1.8) {};
    \draw[MidnightBlue,dashed] (-3,.8) -- node[pos=.1] (A) {} (3,.8);
    \draw[MidnightBlue,dashed] (-3,-.8) -- node[pos=.1] (B) {} (3,-.8);
    \node[MidnightBlue,right,text width=5cm,align=left] (C) at (-2,0) {Beispiele für Energieniveaus gebundener Zustände};
    \draw[MidnightBlue,->] (C) -- (A);
    \draw[MidnightBlue,->] (C) -- (B);
    \draw[Purple,decorate,decoration=brace,text width=3cm,align=right] (-3,.5)
    -- node[left] {Gültigkeitsbereich der Schrödingergleichung} (-3,1.5);
  \end{tikzpicture}
  \caption{Energiespektrum: Lösung der Klein-Gordon-Gleichung im Fall
    eines freien Teilchens.}
  \label{fig:2014-10-14-2}
\end{figure}

%%% Local Variables: 
%%% mode: latex
%%% mode: flyspell
%%% ispell-local-dictionary: "de_DE"
%%% TeX-master: "../QFT.tex"
%%% End: