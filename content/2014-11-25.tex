% Marcel Klett

Damit lautet die Hamiltondichte 
\begin{align}
  \notag
  \mathcal{H} &= \underline{\pi} \underline{\dot{\psi}} + \overline{\pi} \overline{\dot{\psi}} . \mathcal{L} \\ \notag
  &\eqrel{eq:333}{=} \left( \underline{\pi} - \ii \hbar \overline{\psi} \gamma^0 \right) \underline{\dot{\psi}} - \ii \hbar c \overline{\psi} \gamma^l \partial_l \underline{\psi} + mc^2 \overline{\psi} \underline{\psi} \\ \notag
  &= \left( \underline{\pi} - \underline{\pi} \right) \underline{\dot{\psi}} - \ii \hbar c \overline{\psi} \gamma^l \partial_l \underline{\psi} + mc^2 \overline{\psi} \underline{\psi} \\ \notag
  &= - \ii \hbar c \overline{\psi} \gamma^l \partial_l \underline{\psi} + m c^2 \overline{\psi} \underline{\psi}.  
  \label{eq:338}
\end{align}
Auf den ersten Blick hängt die Hamiltondichte nicht von der
Impulsdichte ab. Dies stimmt jedoch nicht ganz, denn wir dürfen
nicht vergessen, dass
\begin{equation}
  \label{eq:339}
  \overline{\psi} \eqrel{eq:337a}{=} \frac{1}{\ii \hbar} \underline{\pi} \gamma^0
\end{equation}
gilt. Somit lautet die Hamiltondichte
\begin{align}
  \notag
  \mathcal{H} &= -c \underline{\pi} \gamma^0 \gamma^l \partial_l \underline{\psi} + \frac{mc^2}{\ii \hbar} \underline{\pi} \gamma^0 \underline{\psi} \\
  \notag
  &= - c \underline{\pi} \alpha^l \partial_l \underline{\psi} + \frac{mc^2}{\ii \hbar} \underline{\pi} \gamma^0 \underline{\psi} \\
  &= \frac{1}{\ii \hbar} \underline{\pi} \underbrace{\gamma^0 \left(-\ii \hbar c \gamma^l \partial_l + mc^2\right)}_{\equiv H_D} \underline{\psi} \mcomma \label{eq:340}
\end{align}
in der wir den Dirac'schen Hamiltonoperator $H_D$ wiedererkennen.

\subsection{Quantisierung}

\paragraph{a) Hamiltonoperator} Für die Quantisierung bietet sich der Weg aus Abschnitt
\ref{sec:313} an. Der erste Schritt besteht dann darin, die Lösungen
der Dirac-Gleichung zu bestimmen. Diese lauten
\begin{align}
  \notag
  \underline{\psi} &= \underline{\psi}^{(+)} + \underline{\psi}^{(-)} \\
  &= \sum_{s = \uparrow , \downarrow} \int \frac{\diff^3 \bm{k}}{(2\pi)^{3/2}} \sqrt{\frac{mc^2}{E_{\bm{k}}}} \left( b(\bm{k},s) \underline{u}(\bm{k},s) \ee^{-\ii k_\mu x^\mu}  + d^*(\bm{k},s) \underline{v}(\bm{k},s) \ee^{\ii k_\mu x^\mu}\right) 
  \label{eq:341a}
\end{align}
mit den Spinoren
\begin{align}
  \label{eq:341b}
  \underline{u} &= \sqrt{\frac{E_{\bm{k}} + mc^2}{2 m c^2}} 
  \begin{pmatrix}
    \chi_s \\
    \frac{c \bm{\sigma} \cdot \bm{p}}{E_{\bm{k}} + mc^2} \; \chi_s 
  \end{pmatrix} \\  
  \label{eq:341c}
  \underline{v} &= \sqrt{\frac{E_{\bm{k}} + mc^2}{2 m c^2}} 
  \begin{pmatrix}
    \frac{c \bm{\sigma} \cdot \bm{p}}{E_{\bm{k}} + mc^2} \; \chi_s  \\
    \chi_s
  \end{pmatrix}
\end{align}
mit der Energie-Impuls-Beziehung 
\[ E_{\bm{k}} = \sqrt{\hbar^2 c^2 \bm{k}^2 + m^2 c^4}\] 
und den Relationen
\begin{align}
  \begin{aligned}
    \overline{u}_s \underline{u}_{s'} &= \underline{v}_s \overline{v}_{s'} = \delta_{s,s'} \mcomma  \\
    \overline{v}_s \underline{u}_{s'} &= \overline{u}_s \underline{v}_{s'} = 0 \mcomma \\
    \underline{u}^\dagger_s \underline{u}_{s'} &= \underline{v}^\dagger_s \underline{v}_{s'} = \frac{E_{\bm{k},s}}{mc^2} \delta_{s,s'}  \mcomma
  \end{aligned}
  \label{eq:341d}
\end{align}
\begin{equation}
  \label{eq:341e}
  \chi_\uparrow =
  \begin{pmatrix}
    1 \\
    0
  \end{pmatrix} \mcomma
  \quad 
  \chi_\downarrow = 
  \begin{pmatrix}
    0 \\
    1
  \end{pmatrix} \mpunkt
\end{equation}

Der nächste Schritt, den wir bestreiten wollen, ist der Übergang zu
Operatoren.
\begin{align}
  \underline{\hat{\psi}} &\eqrel{eq:341a}{=}  \sum_{s = \uparrow , \downarrow} \int \frac{\diff^3 \bm{k}}{(2\pi)^{3/2}} \sqrt{\frac{mc^2}{E_{\bm{k}}}} \left( \hat{b}(\bm{k},s) \underline{u}(\bm{k},s) \ee^{-\ii k_\mu x^\mu}  + \hat{d}^\dagger(\bm{k},s) \underline{v}(\bm{k},s) \ee^{\ii k_\mu x^\mu}\right)  \label{eq:342a}\\
  \underline{\hat{\pi}} &=
  \ii \hbar \hat{\overline{\psi}} \gamma^0 \notag \\
  &\eqrel{eq:342a}{=} \sum_{s = \uparrow , \downarrow} \int \frac{\diff^3 \bm{k}}{(2\pi)^{3/2}} \sqrt{\frac{mc^2}{E_{\bm{k}}}} \left( \hat{b}^\dagger(\bm{k},s) \overline{u}(\bm{k},s) \gamma^0 \ee^{\ii k_\mu x^\mu}  + \hat{d} (\bm{k},s) \overline{v}(\bm{k},s) \gamma^0 \ee^{-\ii k_\mu x^\mu}\right) \label{eq:342b}  
\end{align}
Durch Rechnung folgt der Hamiltonoperator
\begin{equation}
  \label{eq:343}
  H = \int \diff^3 r \mathcal{H} = \int \diff^3 \bm{k} \sum_{s = \uparrow , \downarrow} E_{\bm{k}} (\hat{b}^\dagger(\bm{k},s) \hat{b}(\bm{k},s) - \hat{d}(\bm{k},s) \hat{d}^\dagger(\bm{k},s)) 
\end{equation}

\paragraph{b) Ladung} Analog zu \eqref{eq:330c} beim Klein-Gordon-Feld
gewinnt man aus der Invarianz der Wirkung unter einer globalen
Eichtransformation die Ladungserhaltung,
\begin{subequations}
\begin{equation}
  \label{eq:344a}
  \partial_\mu j^\mu = 0 \quad \text{mit} \quad j^\mu= - \frac{1}{\hbar} \frac{\partial \mathcal{L}}{\partial \underline{\psi}_{,\mu}} \underline{\psi} = c \overline{\psi} \gamma^\mu \underline{\psi}.
\end{equation}
Daraus erfolgt die Erhaltung von
\begin{align}
  Q q &= q \int \diff^3 \bm{r} \frac{j^0}{c} = q \int \diff^3 \bm{r} \overline{\psi} \gamma^0 \underline{\psi} \mcomma \label{eq:344b}  \\
  \hat{Q} q &= \frac{q}{\ii \hbar} \int \diff^3 \bm{r} \hat{\underline{\pi}} \hat{\underline{\psi}} \label{eq:344c} \\
  &= q \int \diff^3 \bm{k} \sum_{s = \uparrow , \downarrow} \left[ \hat{b}^\dagger(\bm{k},s) \hat{b}(\bm{k},s) + \hat{d}(\bm{k},s) \hat{d}^\dagger(\bm{k},s) \right]. \label{eq:344d}
\end{align}
\end{subequations}

\paragraph{c) Quantisierungsregeln} Aus dem Vergleich der
Klein-Gordon-Theorie und den experimentellen Tatsachen wollen wir
erreichen, dass $\hat{b}$ und $\hat{b}^\dagger$ Erzeuger und Vernichter von
Teilchen der Ladung $q$ sind. Außerdem sollen $\hat{d}$ und $\hat{d}^\dagger$
Erzeuger bzw. Vernichter der zugehörigen Antiteilchen sein. Ein
weiteres Problem was uns an der bisherigen Dirac-Theorie stört, ist die
nach unten unbeschränkte Energie. Eine rein positive Energie soll vorliegen. Alle diese Forderungen lassen sich
erfüllen, wenn man fermionische Vertauschungsregeln für
$\underline{\hat{\psi}}$ und $\underline{\hat{\pi}}$ postuliert. Dazu benutzen wir
im Folgenden die Formulierung
\[ \{ \hat{A},\hat{B} \} = \hat{A} \hat{B} + \hat{B} \hat{A} \mpunkt\]
\begin{subequations}
  \begin{align}
    \{\hat{\psi}_\alpha , \hat{\psi}_\beta \} = \{ \hat{\pi}_\alpha, \hat{\pi}_\beta\} &= 0 \label{eq:345a} \\
    \{ \hat{\psi}_\alpha (\bm{r},t), \hat{\pi}_\beta (\bm{r}',t) \} &= \ii \hbar \delta_{\alpha,\beta} \delta^3(\bm{r} - \bm{r}') \label{eq:345b} \\
    \{ \hat{b}(\bm{k},s), \hat{b}(\bm{k}',s)\} =  \{ \hat{d}(\bm{k},s), \hat{d}(\bm{k}',s')\} &=  0 \label{eq:346a1}\\ 
    \{ \hat{d}(\bm{k},s), \hat{b}(\bm{k}',s')\} = \{ \hat{b}(\bm{k},s), \hat{d}^\dagger(\bm{k}',s')\} &=0 \label{eq:346a2} \\
    \{ \hat{b}(\bm{k},s),\hat{b}^\dagger(\bm{k}',s') \} &= \{ \hat{d}(\bm{k},s),\hat{d}^\dagger(\bm{k}',s')\} = \delta_{s,s'} \delta^3(\bm{k} - \bm{k}') \label{eq:346b}
  \end{align}
\end{subequations}
Mit diesen fermionischen Vertauschungsregeln ergibt sich die korrekte
Beschreibung von Elektronen mit der Ladung $q=-e$,
\begin{align}
  \nOrd{H} &= \int \diff^3 \bm{k} \sum_{s = \uparrow , \downarrow} E_{\bm{k}} \left[\hat{b}^\dagger(\bm{k},s) \hat{b}(\bm{k},s) + \hat{d}^\dagger(\bm{k},s) \hat{d}(\bm{k},s) \right], \label{eq:347} \\
  \nOrd{\hat{Q}} &= -e \int \diff^3 \bm{k} \sum_{s = \uparrow , \downarrow} \left[\hat{b}^\dagger(\bm{k},s) \hat{b}(\bm{k},s) -  \hat{d}^\dagger(\bm{k},s) \hat{d}(\bm{k},s) \right] \mpunkt \label{eq:348}
\end{align}

\paragraph{d) Impuls} Für den Impulsoperator findet man
\begin{equation}
  \label{eq:349}
  \nOrd{\bm{p}} = \int \diff^3 \bm{k} \sum_{s = \uparrow , \downarrow} \hbar \bm{k} \left[\hat{b}^\dagger(\bm{k},s) \hat{b}(\bm{k},s) + \hat{d}^\dagger(\bm{k},s) \hat{d}(\bm{k},s)  \right],
\end{equation}
wobei sich die Komponenten des Impulses wie folgt ableiten lassen,
\begin{equation*}
  \hat{p}_l = -\ii \hbar \int \underline{\hat{\psi}}^\dagger \partial_l \underline{\hat{\psi}} \diff^3 \bm{r} \mpunkt
\end{equation*}

\section{Maxwell-Feld}

\subsection{Maxwell-Gleichungen und Eichfreiheit}

Wir definieren den Feldstärketensor
\begin{equation}
  \label{eq:351}
  F^{\mu \nu} =
  \begin{pmatrix}
    0 & - E_x/c & -E_y/c & -E_z/c \\
    E_x/c & 0 & -B_z & B_y \\
    E_y/c & B_z & 0 & -B_x \\
    E_z/c & - B_y & B_x & 0 \\
  \end{pmatrix} \mcomma
\end{equation}
der sich aus den Potentialen \[ A^\mu = \begin{pmatrix} \phi/c \\
  \bm{A} \end{pmatrix} \] via
\begin{equation}
  \label{eq:352}
  F^{\mu \nu} = \partial^\mu A^\nu - \partial^\nu A^\mu
\end{equation}
gewinnen lässt. Damit lauten die Maxwellgleichungen
\begin{subequations}
\begin{align}
  \varepsilon^{\mu \nu \varrho \sigma} \partial_\mu F_{\varrho \sigma} &= 0 \label{eq:353a} \mcomma \\ 
  \partial_\mu F^{\mu \nu} &=\mu_0 j^\nu \mpunkt \label{eq:353b} 
\end{align}
Für freie Felder wird $j^\mu =0$ und wir erhalten
\begin{equation}
  \label{eq:353c}
  \partial_\mu F^{\mu \nu} = 0 \mpunkt
\end{equation}
\end{subequations}
Wie aus der Elektrodynamik bekannt ist, sind die Maxwell-Gleichungen
invariant unter einer Eichtransformation
\begin{equation}
  \label{eq:354}
  A^\mu \leadsto A'^\mu  = A^\mu + \partial^\mu \Lambda \mpunkt
\end{equation}
Für uns wäre es scheinbar sinnvoll, die Lorentz-Eichung
\begin{equation}
  \label{eq:355}
  \partial_\mu A^\mu = 0
\end{equation}
zu verwenden. Damit ist $A^\mu$ jedoch immer noch nicht eindeutig
bestimmt, denn es gilt
\begin{equation}
  \label{eq:356}
  0 = \partial_\mu A'^\mu  = \partial_\mu A^\mu + \quabla \Lambda \mcomma
\end{equation}
was für jedes $\Lambda' = \Lambda + \Lambda_1$ mit $\quabla \Lambda_1
=0$ erfüllt ist. Oft wählt man $\Lambda$ zusätzlich so, dass
\begin{equation}
  \label{eq:357}
  c A^0 = \phi = 0
\end{equation}
erfüllt ist, was uns auf die Coulomb-Eichung
\begin{equation}
  \label{eq:358}
  \div \bm{A} = 0
\end{equation}
führt. Gleichung \eqref{eq:357} und \eqref{eq:358} bilden die
sog. \acct{Strahlungseichung}. Diese ist nicht kovariant,
bietet aber eine einfache Formulierung für die Quantisierung und kann
jederzeit in einem gegebenen Inertialsystem formuliert werden. In der
Strahlungseichung erkennt man, dass das elektromagnetische Feld nur
zwei Freiheitsgrade besitzt, denn $A^\mu$ hat vier Komponenten, jedoch
erhalten wir aus Gleichung \eqref{eq:357} und \eqref{eq:358} zwei
Bedingungen für $A^\mu$. Es gibt in der Strahlungseichung daher nur
die zwei Transversalkomponenten einer sich ausbreitenden Welle zu bestimmen.

\subsection{Kanonische Variablen}

Wir versuchen es mit der Lagrangedichte
\begin{equation}
  \label{eq:359}
  \mathcal{L} = - \frac{1}{4 \mu_0} F_{\mu \nu} F^{\mu \nu}
\end{equation}
für freie Felder.