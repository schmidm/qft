\NeedsTeXFormat{LaTeX2e}

\ProvidesClass{theo-script}

\LoadClassWithOptions{scrbook}

% % %
% % % KOMA Options
% % %

\KOMAoption{open}{right}
\KOMAoption{twoside}{true}
\KOMAoption{listof}{totoc}
\KOMAoption{index}{totoc}
\KOMAoption{numbers}{noenddot}
\KOMAoption{parskip}{half}
\KOMAoption{bibliography}{totoc}
\KOMAoption{cleardoublepage}{empty}

% % %
% % % Common
% % %

\RequirePackage[l2tabu,orthodox]{nag}
\RequirePackage[ngerman]{babel}
\RequirePackage[utf8]{inputenx}
\RequirePackage[T1]{fontenc}
\RequirePackage{currfile}
\RequirePackage{scrhack}
\RequirePackage{geometry}

% % %
% % % Mathematics
% % %

\RequirePackage{mathtools}
\RequirePackage{amsfonts,amssymb}
\RequirePackage{slashed}
\DeclareMathAlphabet\mathds{U}{fplmbb}{m}{n}
% dsfont doesn't work with microtype
\RequirePackage[mathscr]{eucal}
\RequirePackage{braket}
\RequirePackage{siunitx}
\RequirePackage{textcomp}
\sisetup{
  mode = math,
  exponent-product = \cdot,
  number-math-rm = \ensuremath
} % \ensuremath is an ugly hack to get lining figures in siunitx
\allowdisplaybreaks[3]

% % %
% % % Color
% % %

\RequirePackage[dvipsnames,svgnames,x11names]{xcolor}

% % %
% % % Index and Table of contents
% % %

\RequirePackage{imakeidx}

\RequirePackage{tocstyle}
\usetocstyle{nopagecolumn}
\settocstylefeature{pagenumberhook}{\normalfont\color{gray}}

% % %
% % % hyperref
% % %

\RequirePackage{hyperref}
\hypersetup{
  unicode,
  breaklinks=true,
  colorlinks=false,
  pdfborder={0 0 0},
  bookmarksdepth=3,
  pdfdisplaydoctitle=true,
  bookmarksnumbered=true,
}

\numberwithin{equation}{chapter}
\setcounter{secnumdepth}{3}

% % %
% % % Headers
% % %

\RequirePackage{scrpage2}

\def\myHeaderRule{|}
\setkomafont{pageheadfoot}{\normalfont\small\scshape\color{gray}}
\setkomafont{pagenumber}{\normalfont\color{gray}}
\renewcommand{\chaptermark}[1]{\markboth{\llap{\thechapter~\myHeaderRule}~\textls{#1}}{\textls{#1}~\rlap{\myHeaderRule~\thechapter}}}
\renewcommand{\sectionmark}[1]{\markleft{\llap{\thesection~\myHeaderRule}~\textls{#1}}}
\clearscrheadfoot
\rohead[]{\rightmark}
\lehead[]{\leftmark}
\ofoot[\pagemark]{\pagemark}
\ifoot[\tiny\currfilebase]{\tiny\currfilebase}
\pagestyle{scrheadings}
\renewcommand{\chapterpagestyle}{scrheadings}
\renewcommand{\indexpagestyle}{scrheadings}

% % %
% % % Graphic
% % %

\RequirePackage{etex}
\RequirePackage{tikz}
\RequirePackage{tikz-3dplot}
\RequirePackage{tikz-cd}
\RequirePackage{pgfplots}
\RequirePackage{graphicx}
\RequirePackage{tabularx}
\RequirePackage{paracol}
\RequirePackage{booktabs}

\RequirePackage{caption}
\DeclareCaptionFont{gray-bold}{\small\bfseries\color{gray}}
\DeclareCaptionFont{gray-light}{\small\color{gray}}
\DeclareCaptionLabelFormat{number-only}{#2}
\DeclareCaptionFormat{llap}{\llap{#1#2} #3\par}
\DeclareCaptionFormat{triangle}{$\color{gray}\blacktriangleright$ #1#2 #3\par}
\captionsetup{
  figurewithin=none,
  tablewithin=none,
  format=triangle,
  labelformat=number-only,
  labelsep=quad,
  singlelinecheck=no,
  labelfont=gray-light,
  textfont=gray-bold
}

% % %
% % % Fonts
% % %

\addtokomafont{chapter}{\color{MidnightBlue}\bfseries\rmfamily}
\addtokomafont{section}{\bfseries\rmfamily}
\addtokomafont{subsection}{\bfseries\rmfamily}
\addtokomafont{subsubsection}{\bfseries\rmfamily}
\setkomafont{chapterentry}{\color{MidnightBlue}\bfseries\rmfamily}
\setkomafont{descriptionlabel}{\bfseries\rmfamily}
\setkomafont{minisec}{\bfseries\rmfamily}
\setkomafont{paragraph}{\bfseries\rmfamily}

\RequirePackage{titlesec}

\titleformat{\chapter}[hang]
  {\color{MidnightBlue}\Huge\bfseries\boldmath\rmfamily\raggedright}
  {\sbox0{\fontsize{100pt}{120pt}\selectfont \thechapter}\dp0=0pt\box0}
  {1em}
  {}
\titlespacing*{\chapter}{0pt}{-10pt}{15pt}

% % %
% % % Bibliography
% % %

\RequirePackage{csquotes}
\RequirePackage[backend=biber,firstinits=true]{biblatex}
% No URL and DOI in \fullcite{...}
\AtEveryCite{\togglefalse{bbx:doi}\togglefalse{bbx:url}}
